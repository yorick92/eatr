package com.example.yorickbolster.eatr;

import java.util.Date;

public class ChatMessage {

    private String message;
    private String senderId;
    private String date;

    public ChatMessage(String message, String senderId) {
        this.message = message;
        this.senderId = senderId;

        // Initialize to current time
        date = String.valueOf(new Date().getTime());
    }

    public ChatMessage(){

    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
