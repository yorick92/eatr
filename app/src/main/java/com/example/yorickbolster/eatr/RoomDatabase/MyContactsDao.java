package com.example.yorickbolster.eatr.RoomDatabase;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface MyContactsDao {

    @Query("SELECT * FROM MyContacts")
    List<MyContacts> getAll();

    @Query("SELECT * FROM MyContacts WHERE uid IN (:userId)")
    List<MyContacts> loadById(String userId);

    @Query("SELECT * FROM MyContacts WHERE uid IN (:userIds)")
    List<MyContacts> loadByIds(String... userIds);

    @Query("DELETE FROM MyContacts")
    void deleteAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(MyContacts... myContacts);

    @Update
    void updateMyContact(MyContacts... myContacts);

    @Delete
    void delete(MyContacts myContacts);

    @Query("DELETE FROM MyContacts")
    void deleteTable();
}