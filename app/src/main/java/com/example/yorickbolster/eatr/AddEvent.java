package com.example.yorickbolster.eatr;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBufferResponse;
import com.google.android.gms.location.places.PlacePhotoMetadata;
import com.google.android.gms.location.places.PlacePhotoMetadataBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataResponse;
import com.google.android.gms.location.places.PlacePhotoResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class AddEvent extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    TextView showDate, pickedLocation;
    Button createEvent, pickDate, requestLocationsButton, setLocation;
    EditText priceFromEdit, priceToEdit, maxPeopleEdit;
    String finalDate;
    Bitmap placePhoto;
    String locationName, locationAddress, locationId, photoUrlString;
    String restaurantLatitude, restaurantLongitude;
    boolean dateSet, placeSet;
    private static final int PLACE_PICKER_REQUEST = 0;
    ImageView locationPic;
    private StorageReference mStorageRef;
    private FirebaseAuth mAuth;
    private FirebaseDatabase Db;

    int year, month, day, hour, minute;
    int finalYear, finalMonth, finalDay, finalHour, finalMinute;

    List<SuggestedRestaurants> ListOfSuggestions;
    private FusedLocationProviderClient mFusedLocationClient;
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    protected Location mLastLocation;
    Double locationLatitude, locationLongitude;

    class CustomAdapterRestaurantSuggestions extends BaseAdapter {

        @Override
        public int getCount() {
            return ListOfSuggestions.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.list_for_suggested_locations, null);
            TextView restaurantName = view.findViewById(R.id.RestaurantName);
            TextView vicinity = view.findViewById(R.id.vicinity);
            TextView rating = view.findViewById(R.id.rating);
            TextView distance = view.findViewById(R.id.distance);

            restaurantName.setText(ListOfSuggestions.get(i).getRestaurantName());
            vicinity.setText(ListOfSuggestions.get(i).getVicinity());
            rating.setText(ListOfSuggestions.get(i).getRating());
            distance.setText(ListOfSuggestions.get(i).getDistance());


            return view;
        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap bitmap = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                bitmap = BitmapFactory.decodeStream(in);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event);
        final Context appContext = this;
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        final DialogInterface[] custDialog = new DialogInterface[1];
        dateSet = false;
        placeSet = false;

        mStorageRef = FirebaseStorage.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        Db = FirebaseDatabase.getInstance();

        showDate = findViewById(R.id.showPickedDateView);
        pickDate = findViewById(R.id.pickDateButton);
        createEvent = findViewById(R.id.createEventButton);
        pickedLocation = findViewById(R.id.showPickedLocationView);
        setLocation = findViewById(R.id.setLocation);
        requestLocationsButton = findViewById(R.id.requestLocations);
        requestLocationsButton.setEnabled(false);
        locationPic = findViewById(R.id.locationPic);

        setLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                Intent intent;
                try {
                    intent = builder.build(AddEvent.this);
                    startActivityForResult(intent, PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

        requestLocationsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + locationLatitude.toString() + "," + locationLongitude.toString() +
                        "&type=restaurant&rankby=distance&key=AIzaSyCeLVUpx-gIibTYo10URfS1oyKNf72XbmM";
                JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        getListOfSuggestions(appContext, response, custDialog);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("tag", String.valueOf(error));
                    }
                });
                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                queue.add(jsObjRequest);
            }
        });

        pickDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePicker = new DatePickerDialog(AddEvent.this, AddEvent.this, year, month, day);
                datePicker.show();
            }
        });

        createEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean allOkay;
                allOkay = true;
                pickDate = findViewById(R.id.pickDateButton);
                priceFromEdit = findViewById(R.id.priceRangeFromInput);
                priceToEdit = findViewById(R.id.priceRangeToOutput);
                maxPeopleEdit = findViewById(R.id.maxPeopleInput);
                setLocation = findViewById(R.id.setLocation);

                if(!placeSet){
                    setLocation.setError("you have not selected a location yet, please select one here");
                    allOkay = false;
                }

                if(!dateSet){
                    pickDate.setError("you have not set a date yet, please set one here");
                    allOkay = false;
                }

                String priceFromString = priceFromEdit.getText().toString();
                if(TextUtils.isEmpty(priceFromString)){
                    priceFromEdit.setError("You need to set a minimum price here");
                    allOkay = false;
                }
                String priceToString = priceToEdit.getText().toString();
                if(TextUtils.isEmpty(priceToString)){
                    priceToEdit.setError("You need to set a maximum price here");
                    allOkay = false;
                }
                String maxPeopleString = maxPeopleEdit.getText().toString();

                if (allOkay){
                    if(Float.parseFloat(priceFromString) > Float.parseFloat(priceToString)){
                        priceToEdit.setError("Minimum price needs to be higher than maximum price");
                        allOkay = false;
                    }
                }
                if(allOkay){
                    DatabaseReference myRef = Db.getReference("Events");
                    final String id = myRef.push().getKey();

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    placePhoto.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte [] imgData = baos.toByteArray();
                    StorageReference picRef =  mStorageRef.child("pics/" + "Events/" + id + "/eventPic.jpg");
                    picRef.putBytes(imgData).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Log.d("taggy", "image successfully uploaded");
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                        }
                    });

                    Map<String, String> dictionary = new HashMap<String, String>();
                    dictionary.put("eventId", id);
                    dictionary.put("locationName", locationName);
                    dictionary.put("locationAddress", locationAddress);
                    dictionary.put("locationId", locationId);
                    dictionary.put("longitude", restaurantLongitude.toString());
                    dictionary.put("latitude", restaurantLatitude.toString());
                    dictionary.put("priceBegin", priceFromString);
                    dictionary.put("priceEnd", priceToString);
                    dictionary.put("maxPeople", maxPeopleString);
                    dictionary.put("miliseconds", finalDate);
//                    dictionary.put("date", String.valueOf(finalDate.getDate()));
//                    dictionary.put("month", String.valueOf(finalDate.getMonth()));
//                    dictionary.put("year", String.valueOf(finalDate.getYear()+1900));
//                    dictionary.put("hours", String.valueOf(finalDate.getHours()));
//                    dictionary.put("minutes", String.valueOf(finalDate.getMinutes()));
                    myRef.child(id).setValue(dictionary);

                    String userId = mAuth.getCurrentUser().getUid();

                    Map<String, String> joinedPeopleDictionary = new HashMap<String, String>();
                    joinedPeopleDictionary.put(userId, userId);
                    myRef.child(id).child("joinedPeople").setValue(joinedPeopleDictionary);

//                    Intent goToMainPage = new Intent(AddEvent.this, MainPageTabbed.class);
//                    startActivity(goToMainPage);
                    finish();
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!checkPermissions()) {
            requestPermissions();
        } else {
            requestLocationsButton.setEnabled(true);
            getLastLocation();
        }
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION);
        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i("tag", "Displaying permission rationale to provide additional context.");
            showSnackbar(R.string.permission_rationale, android.R.string.ok,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            startLocationPermissionRequest();
                        }
                    });
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            startLocationPermissionRequest();
        }
    }

    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(AddEvent.this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    private void showSnackbar(final int mainTextStringId, final int actionStringId, View.OnClickListener listener) {
        Snackbar.make(findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient.getLastLocation()
            .addOnCompleteListener(this, new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                if (task.isSuccessful() && task.getResult() != null) {
                    mLastLocation = task.getResult();
                    locationLatitude = mLastLocation.getLatitude();
                    locationLongitude = mLastLocation.getLongitude();
                }else {
                    Log.d(TAG, "getLastLocation:exception", task.getException());
                }
                }
            });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSIONS_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    AddEvent.this.recreate(); // recreate event so the location is used.
                } else {
                    Toast.makeText(AddEvent.this, "Set a location or allow the app access to your current location in order to see a list of restaurants", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private void getListOfSuggestions(Context appContext, JSONObject response, final DialogInterface[] custDialog){
        try {
            LinearLayout layout = new LinearLayout(appContext);
            layout.setOrientation(LinearLayout.VERTICAL);
            final ListView suggestionsListView = new ListView(appContext);
            layout.addView(suggestionsListView);

            final AlertDialog.Builder builderSingle = new AlertDialog.Builder(AddEvent.this);
            builderSingle.setTitle("Select One Name:");

            JSONArray array = response.getJSONArray("results");
            ListOfSuggestions = new ArrayList<>();
            for (int i = 0; i <array.length(); i++){
                SuggestedRestaurants rest = new SuggestedRestaurants();
                JSONObject tempJSONObject = array.getJSONObject(i);
//                Log.e("jsonfile", tempJSONObject.toString(4));
                if(tempJSONObject.has("photos")){
                    rest.setPhotoUrlString(tempJSONObject.getJSONArray("photos").getJSONObject(0).getString("photo_reference"));
                }

                if(tempJSONObject.has("name")){
                    rest.setRestaurantName(tempJSONObject.getString("name"));
                }else{
                    rest.setRestaurantName("Name cannot be found");
                }

                if(tempJSONObject.has("place_id")){
                    rest.setLocationId(tempJSONObject.getString("place_id"));
                }else{
                    rest.setLocationId("no id can be found");
                }

                if(tempJSONObject.has("rating")){
                    rest.setRating(tempJSONObject.getString("rating"));
                }else{
                    rest.setRating("no rating");
                }

                if(tempJSONObject.has("vicinity")){
                    rest.setVicinity(tempJSONObject.getString("vicinity"));
                }else{
                    rest.setVicinity("no neighbourhood found");
                }

                if (tempJSONObject.has("geometry"))
                {
                    JSONObject tempJSONObject2 = tempJSONObject.getJSONObject("geometry");
                    if (tempJSONObject2.has("location"))
                    {
                        rest.setLatitude(tempJSONObject2.getJSONObject("location").getString("lat"));
                        rest.setLongitude(tempJSONObject2.getJSONObject("location").getString("lng"));
                    }else{
                        rest.setLatitude("");
                        rest.setLongitude("");
                    }
                }else{
                    rest.setLatitude("");
                    rest.setLongitude("");
                }

                ListOfSuggestions.add(rest);
            }
            CustomAdapterRestaurantSuggestions customAdapter = new CustomAdapterRestaurantSuggestions();
            suggestionsListView.setAdapter(customAdapter);
            suggestionsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    setInfoSelectedPlace(ListOfSuggestions.get(position));
                    custDialog[0].dismiss();
                }
            });
            builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builderSingle.setView(layout);
            custDialog[0] = builderSingle.show();

        } catch (JSONException e) {
            try {
                Log.d("tag", response.toString(4));
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
            Log.d("taggy", e.getMessage());
            e.printStackTrace();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == PLACE_PICKER_REQUEST && resultCode == RESULT_OK){
            Place place = PlacePicker.getPlace(data, this);
            LatLng location = place.getLatLng();
            locationLatitude = location.latitude;
            locationLongitude = location.longitude;
            requestLocationsButton.setEnabled(true);
        }
    }

    void setInfoSelectedPlace(final SuggestedRestaurants suggestedRestaurant){

        // TODO Get real address instead of Vicinity from Google places
        String locationInfo = "Place name: " + suggestedRestaurant.getRestaurantName() + "\nPlace address: " + suggestedRestaurant.getVicinity();
        pickedLocation.setText(locationInfo);

        locationName = suggestedRestaurant.getRestaurantName();
        locationAddress = suggestedRestaurant.getVicinity();
        restaurantLatitude = suggestedRestaurant.getLatitude();
        restaurantLongitude = suggestedRestaurant.getLongitude();
        locationId = suggestedRestaurant.getLocationId();
        placeSet = true;
        if(suggestedRestaurant.getPhotoUrlString() != null){
            String restPhotoRef = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + suggestedRestaurant.getPhotoUrlString() + "&key=AIzaSyCeLVUpx-gIibTYo10URfS1oyKNf72XbmM";
            Log.e("taggy", restPhotoRef);
            try {
                placePhoto = new DownloadImageTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, restPhotoRef).get();
                locationPic.setImageBitmap(placePhoto);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        // TODO add static google map for when there is no picture


    }

    @Override
    public void onDateSet(DatePicker view, int yearLocal, int monthLocal, int dayOfMonthLocal) {
        finalYear = yearLocal;
        finalMonth = monthLocal + 1;
        finalDay = dayOfMonthLocal;

        Calendar c = Calendar.getInstance();
        hour = c.get(Calendar.HOUR);
        minute = c.get(Calendar.MINUTE);

        TimePickerDialog T = new TimePickerDialog(AddEvent.this, AddEvent.this,
                hour, minute, true);
        T.show();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDayLocal, int minuteLocal) {
        finalHour = hourOfDayLocal;
        finalMinute = minuteLocal;
        String message = "Picked date: " + finalDay + "/" + finalMonth + "/" + finalYear + "\n" +
                "Picked time: " + finalHour + ":" + finalMinute;
        showDate.setText(message);

        Calendar calendar = Calendar.getInstance();
        calendar.set(finalYear, finalMonth, finalDay, finalHour, finalMinute, 0);
        finalDate = String.valueOf(calendar.getTimeInMillis());

//        finalDate = new Date(finalYear-1900, finalMonth, finalDay, finalHour, finalMinute, 0);
        dateSet = true;
    }
}