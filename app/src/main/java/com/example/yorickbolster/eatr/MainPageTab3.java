package com.example.yorickbolster.eatr;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yorickbolster.eatr.RoomDatabase.AppDatabase;
import com.example.yorickbolster.eatr.RoomDatabase.MyContacts;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class MainPageTab3 extends Fragment {

    Context context;
    RecyclerView recyclerView;
    RecyclerViewAdapter adapter;
    List<MyContacts> myContactsList;

    AppDatabase dbMyContacts;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_main_page_tab3, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getContext();

        recyclerView = getActivity().findViewById(R.id.ContactsList);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        dbMyContacts = Room.databaseBuilder(context, AppDatabase.class, "mycontacts").build();

        //TODO op het moment worden enkel myContacts opgehaald. Dit uitbreiden met allContacts (presets & groups)
        try {
            myContactsList = new asyncTaskGetMyContacts().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR).get();

            adapter = new RecyclerViewAdapter(myContactsList);

            recyclerView.setAdapter(adapter);

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView contactName, contactCuisines;

        public MyViewHolder(View v) {
            super(v);
            contactName = v.findViewById(R.id.contactName);
            contactCuisines = v.findViewById(R.id.contactCuisines);
        }
    }

    /**
     * A Simple Adapter for the RecyclerView
     */
    public class RecyclerViewAdapter extends RecyclerView.Adapter<MyViewHolder> {
        private List<MyContacts> dataSource;

        public RecyclerViewAdapter(List<MyContacts> dataArgs) {
            dataSource = dataArgs;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_layout_contact, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            holder.contactName.setText(dataSource.get(position).getName());
            holder.contactCuisines.setText(dataSource.get(position).getFavCuis());
            // TODO set profile image

//            Listitem onclicklistener. Komt later van pas, Yorick.
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "Clicked on " + dataSource.get(position).getName(), Toast.LENGTH_SHORT).show();
//                    Intent goToListItem = new Intent(getActivity(), OpenEvent.class);
//                    goToListItem.putExtra("eventId", dataSource.get(position).getEventId());
//                    goToListItem.putExtra("creatorId", dataSource.get(position).getCreator());
//                    startActivity(goToListItem);
                }
            });
        }

        @Override
        public int getItemCount() {
            return dataSource.size();
        }
    }


    public class asyncTaskGetMyContacts extends AsyncTask<Void, Void, List<MyContacts>> {

        @Override
        protected List<MyContacts> doInBackground(Void... voids) {
            List<MyContacts> myContacts = dbMyContacts.myContactsDao().getAll();
            return myContacts;
        }

    }
}