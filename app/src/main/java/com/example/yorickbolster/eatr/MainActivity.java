package com.example.yorickbolster.eatr;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.yorickbolster.eatr.RoomDatabase.MyContacts;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    public CallbackManager mCallbackManager;
    private static final String TAG_FACEBOOK = "FACELOG";
    private static final String TAG_FIREBASE = "FIREBASE_LOG";

//    private FirebaseDatabase Db;
    private Button mFacebookBtn;
    String picUrlString;
    String personName;
    String personEmail;
    String personGender;
    JSONArray friendsArray;

    private FirebaseAuth mAuth;
    private StorageReference mStorageRef;

    FirebaseUser currentUser;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference refUser;

    Context appContext;


    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

        public DownloadImageTask() {

        }

        protected void onPreExecute() {

        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap bitmap = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mStorageRef = FirebaseStorage.getInstance().getReference();
                StorageReference picRef =  mStorageRef.child("pics/Users/" + currentUser.getUid() + " /profile.jpg");
                bitmap = BitmapFactory.decodeStream(in);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte [] data = baos.toByteArray();
                picRef.putBytes(data);
            } catch (Exception e) {
                Log.e("Error", "image download error");
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap result) {

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        appContext = this;

        // Initialize Firebase
        mAuth = FirebaseAuth.getInstance();
//        Db = FirebaseDatabase.getInstance();

        firebaseDatabase = FirebaseDatabase.getInstance();

//        DatabaseReference myRef = Db.getReference("Users").child(currentUser.getUid());



        mFacebookBtn = findViewById(R.id.facebook_login_button);

        // Initialize Facebook Login button
        mCallbackManager = CallbackManager.Factory.create();
        mFacebookBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                LoginManager.getInstance().logInWithReadPermissions(MainActivity.this, Arrays.asList("email", "public_profile", "user_friends"));
                LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.d(TAG_FACEBOOK, "facebook:onSuccess:" + loginResult);

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
//                                        Log.e("loginActivity", response.toString() );
                                        if(object.has("picture")){
                                            try {
                                                Log.d(TAG_FACEBOOK,"JSON response facebook: " + object.toString());
                                                picUrlString = object.getJSONObject("picture").getJSONObject("data").getString("url");
                                                personName = object.getString("name");
                                                personEmail = object.getString("email");
                                                personGender = object.getString("gender");
//                                                Bitmap profilePic = new DownloadImageTask().execute(picUrlString).get();
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                }
                        );

                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id, name, email, gender, birthday, picture.type(large)");
                        request.setParameters(parameters);
                        request.executeAsync();

                        // get friends
                        request = GraphRequest.newGraphPathRequest(
                                loginResult.getAccessToken(),
                                "/"+ loginResult.getAccessToken().getUserId() +"/friends",
                                new GraphRequest.Callback() {
                                    @Override
                                    public void onCompleted(GraphResponse response) {
                                        // Insert your code here
//                                        Log.d("friends", response.toString());
                                        JSONObject object = response.getJSONObject();
                                        try {
                                            friendsArray = object.getJSONArray("data");
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                });

                        request.executeAsync();


                        handleFacebookAccessToken(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                        Log.d(TAG_FACEBOOK, "facebook:onCancel");
                        // ...
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Log.d(TAG_FACEBOOK, "facebook:onError", error);
                        // ...
                    }
                });

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        currentUser = mAuth.getCurrentUser();
        if(currentUser != null) {
            updateUI();
        }

    }

    private void updateUI(){
//        Toast.makeText(MainActivity.this, "you are logged in", Toast.LENGTH_LONG).show();

        Intent goToMainPage = new Intent(MainActivity.this, MainPageTabbed.class);
        startActivity(goToMainPage);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result back to the Facebook SDK
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void handleFacebookAccessToken(final AccessToken token) {
//        Log.e(TAG_FACEBOOK, "handleFacebookAccessToken:" + token);


        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's informations
                            Log.d(TAG_FACEBOOK, "signInWithCredential:success");
                            currentUser = mAuth.getCurrentUser(); // fixes NullPointerException
                            refUser = firebaseDatabase.getReference("Users").child(currentUser.getUid());
                            Calendar calendar = Calendar.getInstance();

                            // Add user to private profile section
                            refUser.child("facebookPicUrl").setValue(picUrlString);
                            refUser.child("facebookId").setValue(token.getUserId());
                            refUser.child("name").setValue(personName);
                            refUser.child("email").setValue(personEmail);
                            refUser.child("gender").setValue(personGender);
                            refUser.child("timeLastChanged").setValue(String.valueOf(calendar.getTimeInMillis()));

                            // Add user to public profile section
                            DatabaseReference refPublicProf = firebaseDatabase.getReference("publicProfiles").child(currentUser.getUid());
                            refPublicProf.child("gender").setValue(personGender);
                            refPublicProf.child("name").setValue(personName);
                            refPublicProf.child("timeLastChanged").setValue(String.valueOf(calendar.getTimeInMillis()));
                            refUser.child("facebookPicUrl").setValue(picUrlString);

                            // TODO is er ook een array mogelijkheid ipv for-loop?
                            DatabaseReference refFacebookIds = firebaseDatabase.getReference("facebookIds");
                            for (int friend = 0; friend < friendsArray.length(); friend++){
                                try {
                                    final int finalFriend = friend; // fix for accessing inner function error at firebase ref update
                                    final String friendId = friendsArray.getJSONObject(friend).get("id").toString();

                                    refFacebookIds.child(friendId).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                        Log.d("dataSnapshot", String.valueOf(dataSnapshot.toString()));
                                        try {
                                            refUser.child("facebookFriends").child(dataSnapshot.getValue().toString()).setValue(friendsArray.getJSONObject(finalFriend).get("name"));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });



                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            try {
                                Bitmap profilePic = new DownloadImageTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, picUrlString).get();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            } catch (ExecutionException e) {
                                e.printStackTrace();
                            }
                            updateUI();

                            // Get facebookfriends from database and store them in local ROOM database
                            Log.d(TAG_FIREBASE, "get fb friends started");

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG_FACEBOOK, "signInWithCredential:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI();
                        }
                    }
                });
    }



}
