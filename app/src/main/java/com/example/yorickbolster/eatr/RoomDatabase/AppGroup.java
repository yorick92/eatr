package com.example.yorickbolster.eatr.RoomDatabase;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

@Entity
public class AppGroup {
    @PrimaryKey @NonNull
    private String id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "adminId")
    private String adminId;

    @ColumnInfo(name = "timeLastChanged")
    private String timeLastChanged;

    @ColumnInfo(name = "contactIds")
    private ArrayList<String> contactIds = new ArrayList<>();

    @Ignore
    private List<AllContacts> contacts;

    @Ignore
    private AllContacts admin;

    public AppGroup(@NonNull String id, String name, String adminId, String timeLastChanged, ArrayList<String> contactIds) {
        this.id = id;
        this.name = name;
        this.adminId = adminId;
        this.timeLastChanged = timeLastChanged;
        this.contactIds = contactIds;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getTimeLastChanged() {
        return timeLastChanged;
    }

    public void setTimeLastChanged(String timeLastChanged) {
        this.timeLastChanged = timeLastChanged;
    }

    public ArrayList<String> getContactIds() {
        return contactIds;
    }

    public void setContactIds(ArrayList<String> contactIds) {
        this.contactIds = contactIds;
    }

    public List<AllContacts> getContacts() {
        return contacts;
    }

    public void setContacts(List<AllContacts> contacts) {
        this.contacts = contacts;
    }

    public AllContacts getAdmin() {
        return admin;
    }

    public void setAdmin(AllContacts admin) {
        this.admin = admin;
    }
}
