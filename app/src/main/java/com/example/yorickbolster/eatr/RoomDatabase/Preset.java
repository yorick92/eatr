package com.example.yorickbolster.eatr.RoomDatabase;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Preset {
    @PrimaryKey @NonNull
    private String id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "timeLastChanged")
    private String timeLastChanged;

    @ColumnInfo(name = "contactIds")
    private ArrayList<String> contactIds = new ArrayList<>();

    @Ignore
    private List<MyContacts> contacts;

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MyContacts> getContacts() {
        return contacts;
    }

    public void setContacts(List<MyContacts> contacts) {
        this.contacts = contacts;
    }

    public ArrayList<String> getContactIds() {
        return contactIds;
    }

    public void setContactIds(ArrayList<String> contactIds) {
        this.contactIds = contactIds;
    }

    public Preset(String id, String name, ArrayList<String> contactIds){
        this.id = id;
        this.name = name;
        this.contactIds = contactIds;
    }

    public String getTimeLastChanged() {
        return timeLastChanged;
    }

    public void setTimeLastChanged(String timeLastChanged) {
        this.timeLastChanged = timeLastChanged;
    }
}