package com.example.yorickbolster.eatr.ImageTransformations;

/**
 * Created by gunna on 17/03/2018.
 */

import android.content.Context;

import jp.co.cyberagent.android.gpuimage.GPUImageBrightnessFilter;
import jp.wasabeef.picasso.transformations.gpu.GPUFilterTransformation;

/**
 * brightness value ranges from -1.0 to 1.0, with 0.0 as the normal level
 */
public class BrightnessFilterTransformation extends GPUFilterTransformation {

    private float mBrightness;

    public BrightnessFilterTransformation(Context context) {
        this(context, 0.0f);
    }

    public BrightnessFilterTransformation(Context context, float brightness) {
        super(context, new GPUImageBrightnessFilter());
        mBrightness = brightness;
        GPUImageBrightnessFilter filter = getFilter();
        filter.setBrightness(mBrightness);
    }

    @Override public String key() {
        return "BrightnessFilterTransformation(brightness=" + mBrightness + ")";
    }
}