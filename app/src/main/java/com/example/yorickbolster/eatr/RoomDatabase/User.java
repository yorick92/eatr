package com.example.yorickbolster.eatr.RoomDatabase;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class User {
    @PrimaryKey @NonNull
    private String uid;

    @ColumnInfo(name = "email")
    private String email;

    @ColumnInfo(name = "facebookFriends")
    private String facebookFriends;

    @ColumnInfo(name = "facebookId")
    private String facebookId;

    @ColumnInfo(name = "facebookPicUrl")
    private String facebookPicUrl;

    @ColumnInfo(name = "favCuis")
    private String favCuis;

    @ColumnInfo(name = "favRest")
    private String favRest;

    @ColumnInfo(name = "gender")
    private String gender;

    @ColumnInfo(name = "groups")
    private String groups;

    @ColumnInfo(name = "joinedEvents")
    private String joinedEvents;

    @ColumnInfo(name = "myEvents")
    private String myEvents;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "picChangedAmount")
    private String picChangedAmount;

    @ColumnInfo(name = "presets")
    private String presets;

    @ColumnInfo(name = "timeLastChanged")
    private String timeLastChanged;


    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFacebookFriends() {
        return facebookFriends;
    }

    public void setFacebookFriends(String facebookFriends) {
        this.facebookFriends = facebookFriends;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getFacebookPicUrl() {
        return facebookPicUrl;
    }

    public void setFacebookPicUrl(String facebookPicUrl) {
        this.facebookPicUrl = facebookPicUrl;
    }

    public String getFavCuis() {
        return favCuis;
    }

    public void setFavCuis(String favCuis) {
        this.favCuis = favCuis;
    }

    public String getFavRest() {
        return favRest;
    }

    public void setFavRest(String favRest) {
        this.favRest = favRest;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGroups() {
        return groups;
    }

    public void setGroups(String groups) {
        this.groups = groups;
    }

    public String getJoinedEvents() {
        return joinedEvents;
    }

    public void setJoinedEvents(String joinedEvents) {
        this.joinedEvents = joinedEvents;
    }

    public String getMyEvents() {
        return myEvents;
    }

    public void setMyEvents(String myEvents) {
        this.myEvents = myEvents;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicChangedAmount() {
        return picChangedAmount;
    }

    public void setPicChangedAmount(String picChangedAmount) {
        this.picChangedAmount = picChangedAmount;
    }

    public String getPresets() {
        return presets;
    }

    public void setPresets(String presets) {
        this.presets = presets;
    }

    public String getTimeLastChanged() {
        return timeLastChanged;
    }

    public void setTimeLastChanged(String timeLastChanged) {
        this.timeLastChanged = timeLastChanged;
    }

    public User(String uid, String email, String facebookFriends, String facebookId, String facebookPicUrl, String favCuis, String favRest, String gender, String groups, String joinedEvents, String myEvents, String name, String picChangedAmount, String presets, String timeLastChanged) {
        this.uid = uid;
        this.email = email;
        this.facebookFriends = facebookFriends;
        this.facebookId = facebookId;
        this.facebookPicUrl = facebookPicUrl;
        this.favCuis = favCuis;
        this.favRest = favRest;
        this.gender = gender;
        this.groups = groups;
        this.joinedEvents = joinedEvents;
        this.myEvents = myEvents;
        this.name = name;
        this.picChangedAmount = picChangedAmount;
        this.presets = presets;
        this.timeLastChanged = timeLastChanged;
    }
}