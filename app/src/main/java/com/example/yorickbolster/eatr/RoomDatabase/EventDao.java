package com.example.yorickbolster.eatr.RoomDatabase;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * Created by yorickbolster on 25/07/2018.
 */

@Dao
public interface EventDao {
    @Query("SELECT * FROM Event")
    List<Event> getAll();

    @Query("SELECT * FROM Event WHERE id IN (:userId)")
    List<Event> loadById(String userId);

    @Query("SELECT * FROM Event WHERE id IN (:userIds)")
    List<Event> loadByIds(String... userIds);

    @Query("DELETE FROM Event")
    void deleteAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Event... events);

    @Update
    void updateMyContact(Event... events);

    @Delete
    void delete(Event event);

    @Query("DELETE FROM Event")
    void deleteTable();
}