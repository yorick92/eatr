package com.example.yorickbolster.eatr.ExpandableRecyclerView;

import com.example.yorickbolster.eatr.EventItem;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class EventType extends ExpandableGroup<EventItem> {

    public EventType(String title, List<EventItem> items) {
        super(title, items);
    }
}
