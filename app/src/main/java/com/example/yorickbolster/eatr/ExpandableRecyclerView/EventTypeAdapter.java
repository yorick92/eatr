package com.example.yorickbolster.eatr.ExpandableRecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.yorickbolster.eatr.EventItem;
import com.example.yorickbolster.eatr.R;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class EventTypeAdapter extends ExpandableRecyclerViewAdapter<EventTypeViewHolder, EventViewHolder> {

    private Context appContext;

    public EventTypeAdapter(List<? extends ExpandableGroup> groups) {
        super(groups);
    }

    @Override
    public EventTypeViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_my_events_head, parent, false);
        return new EventTypeViewHolder(view);
    }

    @Override
    public EventViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        appContext = parent.getContext();
        View view = LayoutInflater.from(appContext).inflate(R.layout.list_layout_event, parent, false);
        return new EventViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(EventViewHolder holder, int flatPosition,
                                      ExpandableGroup group, int childIndex) {

        final EventItem eventItem = ((EventType) group).getItems().get(childIndex);
        holder.onBind(eventItem, appContext);
    }

    @Override
    public void onBindGroupViewHolder(EventTypeViewHolder holder, int flatPosition,
                                      ExpandableGroup group) {
        holder.setEventTypeTitle(group);
    }
}