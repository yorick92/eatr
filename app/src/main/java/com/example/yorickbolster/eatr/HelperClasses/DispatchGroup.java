package com.example.yorickbolster.eatr.HelperClasses;

import android.util.Log;

public class DispatchGroup {

    public int count = 0;
    private Runnable runnable;
    private Boolean hasRun = false;
    public String name = "";

    public DispatchGroup(String name)
    {
        super();
        count = 0;
        this.name = name;
    }

    public synchronized void enter(){
        count++;
    }

    public synchronized void leave(){
        count--;
        notifyGroup();
    }

    public void notify(Runnable r) {
        runnable = r;
        notifyGroup();
    }

    private void notifyGroup(){
        if (count <=0 && runnable!=null && !hasRun) {
            Log.d("DebugIt", "notifyGroup: runs " + name);
            runnable.run();
            hasRun = true;
        }
    }
}