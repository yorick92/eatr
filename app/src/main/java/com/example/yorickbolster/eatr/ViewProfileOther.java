package com.example.yorickbolster.eatr;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yorickbolster.eatr.ImageTransformations.BlurTransformation;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.concurrent.ExecutionException;

public class ViewProfileOther extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private FirebaseDatabase Db;
    private StorageReference mStorageRef;
    ImageView profilePicView, profilePicViewBackground;
    private static final int CAMERA_REQUEST_CODE = 0;
    private static final int GALLERY_REQUEST_CODE = 1;
    private static final int CAMERA_PERMISSION_CODE = 100;
    private ProgressDialog mProgress;
    private String databaseLocationProfilePic;
    private String userId;
    private Context appContext;
//    StorageReference profilePic;

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap bitmap = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mStorageRef = FirebaseStorage.getInstance().getReference();
                StorageReference picRef = mStorageRef.child(databaseLocationProfilePic);
                Log.d("picRef", picRef.getPath().toString());
                bitmap = BitmapFactory.decodeStream(in);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] data = baos.toByteArray();
                picRef.putBytes(data).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        loadPicture();
//                        mProgress.dismiss();
                    }
                });
            } catch (Exception e) {
                Log.e("Error", "image download error");
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return bitmap;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_main_page_tab4);
        appContext = this;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mAuth = FirebaseAuth.getInstance();
        userId = getIntent().getStringExtra("userId");
        databaseLocationProfilePic = "pics/" + "Users/" + userId + "/profile.jpg";

        mStorageRef = FirebaseStorage.getInstance().getReference();
        Db = FirebaseDatabase.getInstance();
//        mProgress = new ProgressDialog(context);
        try {
            new DownloadImageTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, databaseLocationProfilePic).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    private void getProfileData(DataSnapshot snapToCheck, TextView viewToFill, String defaultText) {
        if (snapToCheck.getValue() != null) {
            String theText = snapToCheck.getValue().toString();
            if (theText.equals("")) {
                viewToFill.setText(defaultText);
            } else {
                viewToFill.setText(theText);
            }
        } else {
            viewToFill.setText(defaultText);
        }
    }

    void loadAllTextViews() {
        final TextView nameView = findViewById(R.id.profileName);
        final TextView genderView = findViewById(R.id.genderValueView);
        final TextView favRestView = findViewById(R.id.favPlacesValueView);
        final TextView favCuisView = findViewById(R.id.favCuisinesValueView);

        DatabaseReference myRef = Db.getReference("Users");
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                getProfileData(dataSnapshot.child(userId).child("name"), nameView, "Fill in your name");
                getProfileData(dataSnapshot.child(userId).child("gender"), genderView, "Fill in your gender");
                getProfileData(dataSnapshot.child(userId).child("favRest"), favRestView, "Select a favourite restaurant");
                getProfileData(dataSnapshot.child(userId).child("favCuis"), favCuisView, "Fill in your favourite cuisine");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        // set onclick
        loadAllTextViews();
        profilePicView = findViewById(R.id.profilePic);
        profilePicViewBackground = findViewById(R.id.profilePicBackground);
        profilePicView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO give option to view photo enlarged
                Toast.makeText(appContext, "BE PATIENT! YOU WILL BE ABLE TO VIEW HIS NOSE UP CLOSE SOON", Toast.LENGTH_SHORT).show();
            }
        });

        loadPicture();
    }


    public void loadPicture() {
        mStorageRef.child(databaseLocationProfilePic).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Picasso.with(appContext).load(uri).into(profilePicView);
                Picasso.with(appContext).load(uri).transform(new BlurTransformation(appContext)).into(profilePicViewBackground);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
