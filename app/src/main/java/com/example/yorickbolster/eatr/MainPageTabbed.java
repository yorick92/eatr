package com.example.yorickbolster.eatr;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.Toast;

import com.example.yorickbolster.eatr.AddEventViewPager.AddEvent_ChooseActivity;
import com.example.yorickbolster.eatr.DatabaseHelpers.ContactsDatabaseHelpers;
import com.example.yorickbolster.eatr.DatabaseHelpers.DeleteAllDataHelpers;
import com.example.yorickbolster.eatr.DatabaseHelpers.EventsDatabaseHelpers;
import com.example.yorickbolster.eatr.DatabaseHelpers.GroupDatabaseHelpers;
import com.example.yorickbolster.eatr.DatabaseHelpers.PresetDatabaseHelpers;
import com.example.yorickbolster.eatr.DatabaseHelpers.RoomDataTesters;
import com.example.yorickbolster.eatr.DatabaseHelpers.UserDatabaseHelpers;
import com.example.yorickbolster.eatr.HelperClasses.DispatchGroup;
import com.example.yorickbolster.eatr.Interfaces.RebuildListInterface;
import com.example.yorickbolster.eatr.RoomDatabase.AppDatabase;
import com.example.yorickbolster.eatr.RoomDatabase.MyContacts;
import com.example.yorickbolster.eatr.RoomDatabase.User;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainPageTabbed extends AppCompatActivity {

    private static final String TAG_FIREBASE = "FIREBASE_LOG";

    private Menu menu;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private int numTab;
    private DispatchGroup DispatchContacts;
    private DispatchGroup DispatchRest;

    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private FirebaseDatabase firebaseDatabase;

    private ContactsDatabaseHelpers contactsDatabaseHelpers;
    private UserDatabaseHelpers userDatabaseHelpers;
    private PresetDatabaseHelpers presetDatabaseHelpers;
    private GroupDatabaseHelpers groupDatabaseHelpers;
    private EventsDatabaseHelpers eventsDatabaseHelpers;

    private DatabaseReference refUser;

    AppDatabase dbUser, dbMyContacts, dbAllContacts;

    public FloatingActionButton fab; // needs to be public to reference it from the fragments for scrollbehaviour of recyclerviews
    private Context context;

    private static RebuildListInterface listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page_tabbed);
        context = this;

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();

        firebaseDatabase = FirebaseDatabase.getInstance();

        dbUser = Room.databaseBuilder(context, AppDatabase.class, "user").build();
        dbMyContacts = Room.databaseBuilder(context, AppDatabase.class, "mycontacts").build();
        // TODO all contacts
        dbAllContacts = Room.databaseBuilder(context, AppDatabase.class, "allcontacts").build();

        contactsDatabaseHelpers = new ContactsDatabaseHelpers(context);
        userDatabaseHelpers = new UserDatabaseHelpers(context);
        presetDatabaseHelpers = new PresetDatabaseHelpers(context);
        groupDatabaseHelpers = new GroupDatabaseHelpers(context);
        eventsDatabaseHelpers = new EventsDatabaseHelpers(context);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.selectedTabIndicator));
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                numTab = tab.getPosition();
//                prefs.edit().putInt("numTab", numTab).apply();
                Log.d("viewpager page", String.valueOf(numTab));

                switch (numTab) {
                    case 0:
                        fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_white_24dp));
                        showOption(R.id.action_change_order);
                        break;
                    case 1:
                        fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_white_24dp));
                        hideOption(R.id.action_change_order);
                        break;
                    case 2:
                        fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_directions_bike_white_24dp));
                        hideOption(R.id.action_change_order);
                        break;
                    case 3:
                        fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_mode_edit_white_24dp));
                        hideOption(R.id.action_change_order);
                        break;
                }
            }
        });

        fab = findViewById(R.id.tabFab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent goToAddEvent = new Intent(getActivity(), AddEvent.class);
//                startActivity(goToAddEvent);
                Toast.makeText(MainPageTabbed.this, "FAB CLICKED ON TAB " + String.valueOf(numTab), Toast.LENGTH_SHORT).show();
                switch (numTab) {
                    case 0:
                        // DO STUFF ON EVENTS-TAB
                        // CREATING NEW MEALTIME
                        Intent goToAddEvent = new Intent(MainPageTabbed.this, AddEvent_ChooseActivity.class);
                        startActivity(goToAddEvent);

                        break;
                    case 1:
                        // DO STUFF ON CIRCLES-TAB
                        // CREATE NEW CIRCLE / GROUP
                        break;
                    case 2:
                        // DO STUFF ON MY-EVENTS-TAB
                        // PROBABLY NO FAB NEEDED
                        break;
                    case 3:
                        // DO STUFF ON PROFILE-TAB
                        // EDIT PROFILE
                        break;
                }
            }
        });

        syncRoomDBWithFirebase();
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            if (!isInternetAvailable()) {
                Toast.makeText(MainPageTabbed.this, "Could not connect to the internet, please check your internet connection", Toast.LENGTH_LONG).show();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_logout:
                mAuth.signOut();
                LoginManager.getInstance().logOut();

                updateUI();
                return true;
            case R.id.action_change_order:
                Toast.makeText(context, "This will change the order from date-created to first-upcoming", Toast.LENGTH_LONG).show();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    //*** HELPER ***

    public boolean isInternetAvailable() throws InterruptedException, IOException {
        String command = "ping -c 1 google.com";
        return (Runtime.getRuntime().exec(command).waitFor() == 0);
    }

    //*** UI ***

    public void setFabScrollbehaviour(RecyclerView rv) {
        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && fab.getVisibility() == View.VISIBLE) {
                    fab.hide();
                } else if (dy < 0 && fab.getVisibility() != View.VISIBLE) {
                    fab.show();
                }
            }
        });
    }

    private void hideOption(int id)
    {
        MenuItem item = menu.findItem(id);
        item.setVisible(false);
    }

    private void showOption(int id)
    {
        MenuItem item = menu.findItem(id);
        item.setVisible(true);
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    MainPageTab1 tab1 = new MainPageTab1();
                    listener = tab1;
                    return tab1;
                case 1:
                    MainPageTab2 tab2 = new MainPageTab2();
                    return tab2;
                case 2:
                    MainPageTab3 tab3 = new MainPageTab3();
                    return tab3;
                case 3:
                    MainPageTab4 tab4 = new MainPageTab4();
                    return tab4;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 4;
        }

    }

    private void updateUI() {
        Toast.makeText(MainPageTabbed.this, "you are logged out", Toast.LENGTH_LONG).show();

        Intent goToMainActivity = new Intent(MainPageTabbed.this, MainActivity.class);
        startActivity(goToMainActivity);
        finish();
    }

    //*** DATABASE ***

    final Runnable syncRest = new Runnable() {
        public void run(){
            groupDatabaseHelpers.syncAllGroupData(DispatchRest);
            presetDatabaseHelpers.syncAllPresetData(DispatchRest);
            eventsDatabaseHelpers.syncAllEventData(DispatchRest);
        }
    };

    final Runnable callTab1Function = new Runnable() {
        @Override
        public void run() {
            new RoomDataTesters(context).testAll.run();
            listener.rebuildList(); // rebuild list wordt aangeroepen in
        }
    };

    public void syncRoomDBWithFirebase() {
        // Delete All local data
        new DeleteAllDataHelpers(context).deleteAll.run();

        DispatchContacts = new DispatchGroup("contacts");
        DispatchRest = new DispatchGroup("rest");

        DispatchContacts.enter();
        userDatabaseHelpers.syncUserData(DispatchContacts);
        DispatchContacts.enter();
        contactsDatabaseHelpers.syncMyContactsData(DispatchContacts);

        DispatchRest.enter();
        DispatchRest.enter();
        DispatchRest.enter();
        DispatchContacts.notify(syncRest);
        DispatchRest.notify(callTab1Function);
    }
}
