package com.example.yorickbolster.eatr.RoomDatabase;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface PresetsDao {
    @Query("SELECT * FROM Preset")
    List<Preset> getAll();

    @Query("SELECT * FROM Preset WHERE id IN (:presetId)")
    List<Preset> loadById(String presetId);

    @Query("SELECT * FROM Preset WHERE id IN (:presetIds)")
    List<Preset> loadByIds(String... presetIds);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Preset... presets);

    @Query("DELETE FROM Preset")
    void deleteAll();

    @Update
    void updatePreset(Preset... presets);

    @Delete
    void delete(Preset presets);

    @Query("DELETE FROM Preset")
    void deleteTable();
}