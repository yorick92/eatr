package com.example.yorickbolster.eatr.RoomDatabase;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import java.util.List;


@Dao
public interface PresetsTimeLastChangedDao {
    @Query("SELECT * FROM PresetsTimeLastChanged")
    List<PresetsTimeLastChanged> getAll();

    @Query("DELETE FROM PresetsTimeLastChanged")
    void deleteAll();
}