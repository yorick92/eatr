package com.example.yorickbolster.eatr.RoomDatabase;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yorickbolster on 25/07/2018.
 */

@Entity
public class Event {
    @PrimaryKey
    @NonNull
    private String id;

    @ColumnInfo(name = "creatorId")
    private String creatorId;

    @ColumnInfo(name = "contactIds")
    private ArrayList<String> contactIds = new ArrayList<>();

    @ColumnInfo(name = "groupOrNot")
    private String groupOrNot;

    @ColumnInfo(name = "latitude")
    private String latitude;

    @ColumnInfo(name = "locationAddress")
    private String locationAddress;

    @ColumnInfo(name = "locationId")
    private String locationId;

    @ColumnInfo(name = "locationName")
    private String locationName;

    @ColumnInfo(name = "longitude")
    private String longitude;

    @ColumnInfo(name = "maxPeople")
    private String maxPeople;

    @ColumnInfo(name = "milliseconds")
    private String milliseconds;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "picChangedAmount")
    private String picChangedAmount;

    @ColumnInfo(name = "priceBegin")
    private String priceBegin;

    @ColumnInfo(name = "priceEnd")
    private String priceEnd;

    @ColumnInfo(name = "remarks")
    private String remarks;

    @ColumnInfo(name = "timeLastChanged")
    private String timeLastChanged;

    @ColumnInfo(name = "timeMade")
    private String timeMade;

    @ColumnInfo(name = "typeOfEvent")
    private String typeOfEvent;

    @Ignore
    private List<AllContacts> contacts;

    @Ignore
    private AllContacts creator;


    public Event(@NonNull String id, String creatorId, ArrayList<String> contactIds, String groupOrNot, String latitude, String locationAddress, String locationId, String locationName, String longitude, String maxPeople, String milliseconds, String name, String picChangedAmount, String priceBegin, String priceEnd, String remarks, String timeLastChanged, String timeMade, String typeOfEvent) {
        this.id = id;
        this.creatorId = creatorId;
        this.contactIds = contactIds;
        this.groupOrNot = groupOrNot;
        this.latitude = latitude;
        this.locationAddress = locationAddress;
        this.locationId = locationId;
        this.locationName = locationName;
        this.longitude = longitude;
        this.maxPeople = maxPeople;
        this.milliseconds = milliseconds;
        this.name = name;
        this.picChangedAmount = picChangedAmount;
        this.priceBegin = priceBegin;
        this.priceEnd = priceEnd;
        this.remarks = remarks;
        this.timeLastChanged = timeLastChanged;
        this.timeMade = timeMade;
        this.typeOfEvent = typeOfEvent;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public ArrayList<String> getContactIds() {
        return contactIds;
    }

    public void setContactIds(ArrayList<String> contactIds) {
        this.contactIds = contactIds;
    }

    public String getGroupOrNot() {
        return groupOrNot;
    }

    public void setGroupOrNot(String groupOrNot) {
        this.groupOrNot = groupOrNot;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLocationAddress() {
        return locationAddress;
    }

    public void setLocationAddress(String locationAddress) {
        this.locationAddress = locationAddress;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getMaxPeople() {
        return maxPeople;
    }

    public void setMaxPeople(String maxPeople) {
        this.maxPeople = maxPeople;
    }

    public String getMilliseconds() {
        return milliseconds;
    }

    public void setMilliseconds(String milliseconds) {
        this.milliseconds = milliseconds;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicChangedAmount() {
        return picChangedAmount;
    }

    public void setPicChangedAmount(String picChangedAmount) {
        this.picChangedAmount = picChangedAmount;
    }

    public String getPriceBegin() {
        return priceBegin;
    }

    public void setPriceBegin(String priceBegin) {
        this.priceBegin = priceBegin;
    }

    public String getPriceEnd() {
        return priceEnd;
    }

    public void setPriceEnd(String priceEnd) {
        this.priceEnd = priceEnd;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTimeLastChanged() {
        return timeLastChanged;
    }

    public void setTimeLastChanged(String timeLastChanged) {
        this.timeLastChanged = timeLastChanged;
    }

    public String getTimeMade() {
        return timeMade;
    }

    public void setTimeMade(String timeMade) {
        this.timeMade = timeMade;
    }

    public String getTypeOfEvent() {
        return typeOfEvent;
    }

    public void setTypeOfEvent(String typeOfEvent) {
        this.typeOfEvent = typeOfEvent;
    }

    public List<AllContacts> getContacts() {
        return contacts;
    }

    public void setContacts(List<AllContacts> contacts) {
        this.contacts = contacts;
    }

    public AllContacts getCreator() {
        return creator;
    }

    public void setCreator(AllContacts creator) {
        this.creator = creator;
    }
}
