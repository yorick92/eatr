package com.example.yorickbolster.eatr;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OpenEvent extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private DatabaseReference databaseEvent;
    private StorageReference mStorageRef;
    private String location;
    private String priceBegin;
    private String priceEnd;
    private String maxPeople;
    private String eventId;
    private String userId;
    private String creatorId, creatorName;
    private boolean userJoined;
    TextView namePlaceView, priceRangeView, maxPeopleView, dateView, timeView;
    TextView invitationTextView, creatorNameView;
    Button locationButton, joinLeaveButton, attendButton, declineButton;
    RelativeLayout chatCard, creatorLayout;
    LinearLayout invitationLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_event);

//        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar((android.support.v7.widget.Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeButtonEnabled(true);

        invitationTextView = findViewById(R.id.invitationTextView);
        namePlaceView = findViewById(R.id.restaurantNameView);
        locationButton = findViewById(R.id.openEventLocation);
        priceRangeView = findViewById(R.id.costView);
        maxPeopleView = findViewById(R.id.openEventmaxPeople);
        dateView = findViewById(R.id.dateView);
        timeView = findViewById(R.id.timeView);
        joinLeaveButton = findViewById(R.id.buttonJoinEvent);

        creatorNameView = findViewById(R.id.creatorNameView);

        chatCard = findViewById(R.id.chatCard);
        invitationLayout = findViewById(R.id.invitationLayout);
        creatorLayout = findViewById(R.id.creatorLayout);
        attendButton = findViewById(R.id.attendButton);
        declineButton = findViewById(R.id.declineButton);

        mAuth = FirebaseAuth.getInstance();
        userId = mAuth.getCurrentUser().getUid();
        eventId = getIntent().getStringExtra("eventId");
        creatorId = getIntent().getStringExtra("creatorId");

        creatorNameView.setText(creatorId);

        databaseEvent = FirebaseDatabase.getInstance().getReference("Events").child(eventId);
        mStorageRef = FirebaseStorage.getInstance().getReference();
        loadPictureEvent();
        loadPictureEventCreator();

        chatCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(OpenEvent.this, "Opens chat-window", Toast.LENGTH_SHORT).show();

                Intent openChat = new Intent(OpenEvent.this, OpenEventChat.class);
                openChat.putExtra("eventId", eventId);
                startActivity(openChat);
            }
        });

        creatorLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent viewUser = new Intent(OpenEvent.this, ViewProfileOther.class);
                viewUser.putExtra("userId", creatorId);
                startActivity(viewUser);
            }
        });

        joinLeaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!userJoined) {
                    databaseEvent.child("joinedPeople").child(userId).setValue(userId);
                    setJoinLeaveStatus(true);
                } else {
                    databaseEvent.child("joinedPeople").child(userId).removeValue();
                    setJoinLeaveStatus(false);
                }
            }
        });

        locationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseEvent.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.e("taggy", dataSnapshot.child("locationId").getValue().toString());
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        String latitude = dataSnapshot.child("latitude").getValue().toString();
                        String longitude = dataSnapshot.child("longitude").getValue().toString();
                        i.setData(Uri.parse("http://maps.google.com/maps?daddr=" + latitude + "," + longitude));
                        i.setPackage("com.google.android.apps.maps");
                        startActivity(i);

//                        Uri gmmIntentUri = Uri.parse("geo:" + latitude + "," + longitude);
//                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//                        mapIntent.setPackage("com.google.android.apps.maps");
//                        startActivity(mapIntent);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });
    }

    public void loadPictureEvent() {
        final ImageView openEventPhoto = findViewById(R.id.imageEventCollapsing);
        final ImageView eventThumbnail = findViewById(R.id.restaurantImageView);
        mStorageRef.child("pics/" + "Events/" + eventId + "/eventPic.jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Picasso.with(getApplicationContext()).load(uri).into(openEventPhoto);
                Picasso.with(getApplicationContext()).load(uri).into(eventThumbnail);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
    }

    public void loadPictureEventCreator() {
        final ImageView creatorPhotoInvitation = findViewById(R.id.imageCreator);
        final ImageView creatorPhotoDetails = findViewById(R.id.creatorImageView);

        mStorageRef.child("pics/" + "Users/" + creatorId + "/profile.jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Picasso.with(getApplicationContext()).load(uri).into(creatorPhotoInvitation);
                Picasso.with(getApplicationContext()).load(uri).into(creatorPhotoDetails);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void setJoinLeaveStatus(Boolean attending) {
        if (attending.equals(true)) {
            joinLeaveButton.setText("Leave event");
            userJoined = true;
            chatCard.setVisibility(View.VISIBLE);
            invitationLayout.setVisibility(View.GONE);
            // TODO set attendButton onclicklistener to null
            attendButton.setEnabled(false);
            attendButton.setText(R.string.button_attend_text_user_attends);
            declineButton.setVisibility(View.VISIBLE);
            declineButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(OpenEvent.this, "You clicked declineButton", Toast.LENGTH_SHORT).show();
                    databaseEvent.child("joinedPeople").child(userId).removeValue();
                }
            });
            // TODO set text attendButton to 'You are attending'
        } else {
            joinLeaveButton.setText("Join event");
            userJoined = false;
            chatCard.setVisibility(View.GONE);
            invitationLayout.setVisibility(View.VISIBLE);
            attendButton.setText(R.string.button_attend_default_text);
            attendButton.setEnabled(true);
            // TODO set attendButton onclicklistener correctly
            attendButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(OpenEvent.this, "You clicked attendButton", Toast.LENGTH_SHORT).show();
                    databaseEvent.child("joinedPeople").child(userId).setValue(userId);
                }
            });
            // TODO check to see if declinebutton needs to be invisible (on first visit / after decline)
            declineButton.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        databaseEvent.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.child("joinedPeople").hasChild(userId)) {
                    setJoinLeaveStatus(true);
                } else {
                    setJoinLeaveStatus(false);
                }

                location = dataSnapshot.child("locationName").getValue().toString();
                priceBegin = dataSnapshot.child("priceBegin").getValue().toString();
                priceEnd = dataSnapshot.child("priceEnd").getValue().toString();
                maxPeople = dataSnapshot.child("maxPeople").getValue().toString();
//                creatorId = dataSnapshot.child("creator").getValue().toString(); // wordt al vervuld door putExtra intent
//                creatorName = dataSnapshot.child() // id vertalen naar naam
                long a = Long.parseLong(dataSnapshot.child("miliseconds").getValue().toString());
                Date date = new Date(a);
//                eventDate = new Date();
//                eventDate.setDate(Integer.parseInt(dataSnapshot.child("date").getValue().toString()));
//                eventDate.setHours(Integer.parseInt(dataSnapshot.child("hours").getValue().toString()));
//                eventDate.setMinutes(Integer.parseInt(dataSnapshot.child("minutes").getValue().toString()));
//                eventDate.setMonth(Integer.parseInt(dataSnapshot.child("month").getValue().toString()));
//                eventDate.setYear(Integer.parseInt(dataSnapshot.child("year").getValue().toString())-1900);

                DateFormat dfDate = new SimpleDateFormat("EEE dd MMMM yyyy");
                DateFormat dfTime = new SimpleDateFormat("HH:mm");

                CollapsingToolbarLayout collapsingToolbarLayout = findViewById(R.id.collapsingToolbarLayout);
                collapsingToolbarLayout.setTitle(location);

                invitationTextView.setText(creatorId + " " + getResources().getString(R.string.event_invitation_message));

                locationButton.setText("location: " + location);
                namePlaceView.setText(location);
//                getSupportActionBar().setTitle(location);
//                setTitle(location);

                if (priceBegin.equals("-1")) {
                    TextView priceRangeViewUnderline = findViewById(R.id.costUnderlineView);
                    priceRangeViewUnderline.setText("No estimate given");
                    priceRangeView.setVisibility(View.GONE);
                } else {
                    priceRangeView.setText(priceBegin + " - " + priceEnd);
                }
                maxPeopleView.setText("Max people: " + maxPeople);
                dateView.setText(dfDate.format(date));
                timeView.setText(dfTime.format(date));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
