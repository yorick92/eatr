package com.example.yorickbolster.eatr.AddEventViewPager;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yorickbolster.eatr.AddEventViewPager.ScreenSlidePageFragment1;
import com.example.yorickbolster.eatr.AddEventViewPager.ScreenSlidePageFragment2;
import com.example.yorickbolster.eatr.R;

public class AddEvent_ViewPaging extends AppCompatActivity {
    /**
     * The number of pages (wizard steps) to show in this demo.
     */
    private static final int NUM_PAGES = 5;

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager mPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;
    private ImageView pageImage;
    private TextView pageTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event_viewpaging);

        // Instantiate a ViewPager and a PagerAdapter.
        mPager = findViewById(R.id.add_event_pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);

        pageImage = findViewById(R.id.pageImage);
        pageTitle = findViewById(R.id.pageTitle);
        final Button buttonPrev = findViewById(R.id.buttonPrevious);
        final Button buttonNext = findViewById(R.id.buttonNext);

        buttonPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPager.setCurrentItem(getItem(- 1), true);
            }
        });
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPager.setCurrentItem(getItem(+ 1), true);
            }
        });


        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                switch (position) {
//                    case 0:
//                        pageImage.setImageDrawable(getResources().getDrawable(R.drawable.yorick));
//                    case 1:
//                        pageImage.setImageDrawable(getResources().getDrawable(R.drawable.hussain));
//                }

                buttonPrev.setEnabled(mPager.getCurrentItem() != 0); // disables prev button on first fragment
                if (mPager.getCurrentItem() == mPagerAdapter.getCount() - 1) {
                    // tekst van button veranderen naar FINISH
                    buttonNext.setText(R.string.button_finish);
                }
                else {
                    buttonNext.setText(R.string.button_next);
                }

                if (position == 0) {
                    pageImage.setImageDrawable(getResources().getDrawable(R.drawable.yorick));
                    pageTitle.setText("Page 1");
                }
                else if (position == 1) {
                    pageImage.setImageDrawable(getResources().getDrawable(R.drawable.hussain));
                    pageTitle.setText("Page 2");
                }
                else if (position == 2) {
                    pageImage.setImageDrawable(getResources().getDrawable(R.drawable.papito));
                    pageTitle.setText("Page 3");
                }
                else if (position == 3) {
                    pageImage.setImageDrawable(getResources().getDrawable(R.drawable.profile_icon));
                    pageTitle.setText("Page 4");
                }
                else if (position == 4) {
                    pageImage.setImageDrawable(getResources().getDrawable(R.drawable.yorick));
                    pageTitle.setText("Page 5");
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });






        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private int getItem(int i) {
        return mPager.getCurrentItem() + i;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }
    }

    /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment1 objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new ScreenSlidePageFragment1();
                case 1:
                    return new ScreenSlidePageFragment2();
                default:
                    return new ScreenSlidePageFragment1();
            }
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }
}
