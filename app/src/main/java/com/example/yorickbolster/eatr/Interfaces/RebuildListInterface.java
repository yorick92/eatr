package com.example.yorickbolster.eatr.Interfaces;

/**
 * Created by yorickbolster on 25/07/2018.
 */

public interface RebuildListInterface {
    void rebuildList();
}