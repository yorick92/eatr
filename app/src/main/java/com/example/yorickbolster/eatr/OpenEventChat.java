package com.example.yorickbolster.eatr;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class OpenEventChat extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private DatabaseReference databaseEventChat;
    private FirebaseDatabase Db;
    private StorageReference mStorageRef;
    private String eventId;
    private String userId;

    Context appContext;

    private MessageListAdapter mMessageAdapter;

    List<ChatMessage> listOfChatMessages;
    RecyclerView listOfMessages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_event_chat);

        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        // TODO titel veranderen naar naam evenement
        actionbar.setTitle("Chat");

        appContext = this;

        mAuth = FirebaseAuth.getInstance();
        userId = mAuth.getCurrentUser().getUid();
        eventId = getIntent().getStringExtra("eventId");
        databaseEventChat = FirebaseDatabase.getInstance().getReference("eventChats").child(eventId);
        mStorageRef = FirebaseStorage.getInstance().getReference();


//        databaseEventChat.child("joinedPeople").child(userId).setValue(userId);
        FloatingActionButton fab = findViewById(R.id.fabSendMessage);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText input = findViewById(R.id.messageInputEditText);

                if (input.getText().toString().trim().length() > 0) {

                    // Read the input field and push a new instance
                    // of ChatMessage to the Firebase database
                    databaseEventChat
                            .push()
                            .setValue(new ChatMessage(input.getText().toString(),
                                    FirebaseAuth.getInstance()
                                            .getCurrentUser()
                                            .getUid())
                            );
                    // TODO autoscroll naar laatste comment. de recycleview getchildrencount geeft int = 6 (het totaal getoonde children)

                    // Clear the input
                    input.setText("");
                }
            }
        });

    }

//    public static class ChatReceivedHolder extends RecyclerView.ViewHolder {
//        // each data item is just a string in this case
//        TextView messageText, messageUser, messageTime;
//
//        public ChatReceivedHolder(View v) {
//            super(v);
//            messageText = v.findViewById(R.id.message_text);
//            messageUser = v.findViewById(R.id.user_id);
//            messageTime = v.findViewById(R.id.message_date);
//        }
//    }

    public void displayChatMessages(List<ChatMessage> messageList) {
//        Query query = databaseEventChat;
//        FirebaseRecyclerOptions<ChatMessage> options = new FirebaseRecyclerOptions.Builder<ChatMessage>()
//                .setQuery(query, ChatMessage.class)
//                .build();




        final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        listOfMessages.setLayoutManager(layoutManager);

        mMessageAdapter = new MessageListAdapter(this, messageList);
//        Log.e("list of messages", messageList.get(0).getMessage()+"");
        listOfMessages.setAdapter(mMessageAdapter);
    }

    public class MessageListAdapter extends RecyclerView.Adapter {
        private static final int VIEW_TYPE_MESSAGE_SENT = 1;
        private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;

        private Context mContext;
        private List<ChatMessage> mMessageList;

        public MessageListAdapter(Context context, List<ChatMessage> messageList) {
            mContext = context;
            mMessageList = messageList;
        }

        @Override
        public int getItemCount() {
            return mMessageList.size();
        }

        // Determines the appropriate ViewType according to the sender of the message.
        @Override
        public int getItemViewType(int position) {
            ChatMessage message = (ChatMessage) mMessageList.get(position);

            if (message.getSenderId().equals(mAuth.getCurrentUser().getUid())) {
                // If the current user is the sender of the message
                return VIEW_TYPE_MESSAGE_SENT;
            } else {
                // If some other user sent the message
                return VIEW_TYPE_MESSAGE_RECEIVED;
            }
        }

        // Inflates the appropriate layout according to the ViewType.
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view;

            if (viewType == VIEW_TYPE_MESSAGE_SENT) {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.listitem_chat_sent, parent, false);
                return new SentMessageHolder(view);
            } else if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.listitem_chat_received, parent, false);
                return new ReceivedMessageHolder(view);
            }

            return null;
        }

        // Passes the message object to a ViewHolder so that the contents can be bound to UI.
        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ChatMessage message = (ChatMessage) mMessageList.get(position);

            switch (holder.getItemViewType()) {
                case VIEW_TYPE_MESSAGE_SENT:
                    ((SentMessageHolder) holder).bind(message);
                    break;
                case VIEW_TYPE_MESSAGE_RECEIVED:
                    ((ReceivedMessageHolder) holder).bind(message);
            }
        }

        private class SentMessageHolder extends RecyclerView.ViewHolder {
            TextView messageText, timeText;

            SentMessageHolder(View itemView) {
                super(itemView);

                messageText = (TextView) itemView.findViewById(R.id.message_text);
                timeText = (TextView) itemView.findViewById(R.id.message_date);
            }

            void bind(ChatMessage message) {
                messageText.setText(message.getMessage());

                // Format the stored timestamp into a readable String using method.
                timeText.setText(message.getDate());
            }
        }

        private class ReceivedMessageHolder extends RecyclerView.ViewHolder {
            TextView messageText, timeText, nameText;
            ImageView profileImage;

            ReceivedMessageHolder(View itemView) {
                super(itemView);

                messageText = (TextView) itemView.findViewById(R.id.message_text);
                timeText = (TextView) itemView.findViewById(R.id.message_date);
                nameText = (TextView) itemView.findViewById(R.id.user_id);
                profileImage = (ImageView) itemView.findViewById(R.id.user_image);
            }

            void bind(ChatMessage message) {
                messageText.setText(message.getMessage());

                // Format the stored timestamp into a readable String using method.
                timeText.setText(message.getDate());

                nameText.setText(message.getSenderId());

                // Insert the profile image from the URL into the ImageView.
//                Picasso.with(this).load(uri).into(profileImage);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
//        adapter.startListening();

        listOfChatMessages = new ArrayList<>();

        databaseEventChat.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listOfChatMessages.clear();
                for (DataSnapshot eventSnapShot : dataSnapshot.getChildren()) {
                    ChatMessage currentChatItem = new ChatMessage();
                    currentChatItem.setDate(eventSnapShot.child("date").getValue().toString());
                    currentChatItem.setMessage(eventSnapShot.child("message").getValue().toString());
                    currentChatItem.setSenderId(eventSnapShot.child("senderId").getValue().toString());

                    listOfChatMessages.add(currentChatItem);
                }
                Log.d("aantal messages", String.valueOf(listOfChatMessages.size()));

                listOfMessages = findViewById(R.id.messageList);
                displayChatMessages(listOfChatMessages);


//                LinearLayoutManager myLayoutManager = (LinearLayoutManager) listOfMessages.getLayoutManager();
//                int scrollPosition = myLayoutManager.findLastVisibleItemPosition();
//
//                if (scrollPosition == listOfChatMessages.size()-1) {
//                    Toast.makeText(context, "scrollPosition: " + scrollPosition + "\nlistOfChatMessages.size: " + listOfChatMessages.size(), Toast.LENGTH_SHORT).show();
//                    listOfMessages.smoothScrollToPosition(listOfChatMessages.size());
//                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



    }

    @Override
    protected void onStop() {
        super.onStop();
//        adapter.stopListening();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
