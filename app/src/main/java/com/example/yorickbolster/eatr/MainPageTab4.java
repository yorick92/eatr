package com.example.yorickbolster.eatr;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yorickbolster.eatr.ImageTransformations.BlurTransformation;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.concurrent.ExecutionException;

import static android.app.Activity.RESULT_OK;

public class MainPageTab4 extends Fragment {

    private FirebaseAuth mAuth;
    private FirebaseDatabase Db;
    private StorageReference mStorageRef;
    ImageView profilePicView, profilePicViewBackground;
    private static final int CAMERA_REQUEST_CODE = 0;
    private static final int GALLERY_REQUEST_CODE = 1;
    private static final int CAMERA_PERMISSION_CODE = 100;
    private ProgressDialog mProgress;
    private String databaseLocationProfilePic;
    private Context appContext;
//    StorageReference profilePic;

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap bitmap = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mStorageRef = FirebaseStorage.getInstance().getReference();
                StorageReference picRef = mStorageRef.child(databaseLocationProfilePic);
                bitmap = BitmapFactory.decodeStream(in);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] data = baos.toByteArray();
                picRef.putBytes(data).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        loadPicture();
                        mProgress.dismiss();
                    }
                });
            } catch (Exception e) {
                Log.e("Error", "image download error");
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return bitmap;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_page_tab4, container, false);
        mAuth = FirebaseAuth.getInstance();
        databaseLocationProfilePic = "pics/" + "Users/" + mAuth.getCurrentUser().getUid() + " /profile.jpg";
        mStorageRef = FirebaseStorage.getInstance().getReference();
        Db = FirebaseDatabase.getInstance();
        mProgress = new ProgressDialog(getContext());

        appContext = getActivity();
        return rootView;
    }

    private void getProfileData(DataSnapshot snapToCheck, TextView viewToFill, String defaultText) {
        if (snapToCheck.getValue() != null) {
            String theText = snapToCheck.getValue().toString();
            if (theText.equals("")) {
                viewToFill.setText(defaultText);
            } else {
                viewToFill.setText(theText);
            }
        } else {
            viewToFill.setText(defaultText);
        }
    }

    void loadSingleTextView(int id, String nameInDatabase, String defaultText) {
        final TextView tv = getView().findViewById(id);
        final String childName = nameInDatabase;
        final String defaultTextForView = defaultText;
        DatabaseReference myRef = Db.getReference("Users");
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                getProfileData(dataSnapshot.child(mAuth.getCurrentUser().getUid()).child(childName), tv, defaultTextForView);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    void loadAllTextViews() {
        final TextView nameView = getView().findViewById(R.id.profileName);
        final TextView genderView = getView().findViewById(R.id.genderValueView);
        final TextView favRestView = getView().findViewById(R.id.favPlacesValueView);
        final TextView favCuisView = getView().findViewById(R.id.favCuisinesValueView);

        DatabaseReference myRef = Db.getReference("Users");
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                getProfileData(dataSnapshot.child(mAuth.getCurrentUser().getUid()).child("name"), nameView, "Fill in your name");
                getProfileData(dataSnapshot.child(mAuth.getCurrentUser().getUid()).child("gender"), genderView, "Fill in your gender");
                getProfileData(dataSnapshot.child(mAuth.getCurrentUser().getUid()).child("favRest"), favRestView, "Select a favourite restaurant");
                getProfileData(dataSnapshot.child(mAuth.getCurrentUser().getUid()).child("favCuis"), favCuisView, "Fill in your favourite cuisine");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        // set onclick
        loadAllTextViews();
        setOnTouchEditProfile();
        profilePicView = getView().findViewById(R.id.profilePic);
        profilePicViewBackground = getView().findViewById(R.id.profilePicBackground);
        profilePicView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Choose an option");
                builder.setItems(new CharSequence[]
                                {"Take new picture", "Select from gallery", "Use Facebook picture", "Delete picture"},
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // The 'which' argument contains the index position
                                // of the selected item
                                switch (which) {
                                    case 0:
                                        if (ActivityCompat.checkSelfPermission(appContext, Manifest.permission.CAMERA)
                                                == PackageManager.PERMISSION_DENIED) {
                                            requestPermissions(new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_CODE);
                                        } else {
                                            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                            startActivityForResult(cameraIntent, CAMERA_REQUEST_CODE);
                                        }
                                        break;
                                    case 1:
                                        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                                        startActivityForResult(galleryIntent, GALLERY_REQUEST_CODE);
                                        break;
                                    case 2:
                                        facebookPic();
                                        break;
                                    case 3:
                                        deleteProfilePic();
                                        break;
                                }
                            }
                        });
                builder.create().show();
            }
        });

        loadPicture();
    }

    public void deleteProfilePic() {
        StorageReference picRef = mStorageRef.child(databaseLocationProfilePic);
        picRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                profilePicView.setImageResource(R.drawable.profile_icon);
                profilePicViewBackground.setImageResource(R.drawable.profile_icon);
                Toast.makeText(getContext(), "picture deleted from Eatr database", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void facebookPic() {
        mProgress.setMessage("getting your facebook profile picture");
        mProgress.show();
        DatabaseReference myRef = Db.getReference("Users");
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String picUrlString = dataSnapshot.child(mAuth.getCurrentUser().getUid()).child("facebookPicUrl").getValue().toString();
                setFacebookPic(picUrlString);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setFacebookPic(String picUrlString) {
        try {
            Bitmap profilePic = new DownloadImageTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, picUrlString).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public void loadPicture() {
        mStorageRef.child(databaseLocationProfilePic).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Picasso.with(getContext()).load(uri).into(profilePicView);
                Picasso.with(getContext()).load(uri).transform(new BlurTransformation(getActivity())).into(profilePicViewBackground);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Log.e("taggy", "ayy");
        switch (requestCode) {
            case CAMERA_PERMISSION_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST_CODE);
                } else {
                    Toast.makeText(appContext, "You denied the camera permission", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {
            mProgress.setMessage("uploading photo");
            mProgress.show();

            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] imgData = baos.toByteArray();
            StorageReference picRef = mStorageRef.child(databaseLocationProfilePic);
            picRef.putBytes(imgData).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    loadPicture();
                    mProgress.dismiss();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                }
            });
        } else if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK) {
            mProgress.setMessage("uploading photo");
            mProgress.show();

            Uri imageUri = data.getData();
            InputStream imageStream;
            try {
                imageStream = getActivity().getApplicationContext().getContentResolver().openInputStream(imageUri);
                Bitmap bitmap = BitmapFactory.decodeStream(imageStream);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] imgData = baos.toByteArray();
                StorageReference picRef = mStorageRef.child(databaseLocationProfilePic);
                picRef.putBytes(imgData).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        loadPicture();
                        mProgress.dismiss();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }
    }

    void setOnTouchEditProfile() {
        final TextView nameTextView = getView().findViewById(R.id.profileName);
        final TextView genderTextView = getView().findViewById(R.id.genderValueView);
        final TextView favRestTextView = getView().findViewById(R.id.favPlacesValueView);
        final TextView favCuisTextView = getView().findViewById(R.id.favCuisinesValueView);
        Button editProfileButton = getView().findViewById(R.id.editProfileButton);
        editProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Edit your profile");

                LinearLayout layout = new LinearLayout(getContext());
                layout.setOrientation(LinearLayout.VERTICAL);

                final TextView nameView = new TextView(getContext());
                nameView.setText("Name:");
                layout.addView(nameView);
                final EditText nameInputView = new EditText(getContext());
                nameInputView.setFilters(new InputFilter[]{new InputFilter.LengthFilter(27)});
                nameInputView.setInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
                nameInputView.setText(nameTextView.getText());
                layout.addView(nameInputView);

                final TextView genderView = new TextView(getContext());
                genderView.setText("Gender:");
                layout.addView(genderView);
                final EditText genderInputView = new EditText(getContext());
                genderInputView.setFilters(new InputFilter[]{new InputFilter.LengthFilter(12)});
                genderInputView.setInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
                genderInputView.setText(genderTextView.getText());
                layout.addView(genderInputView);

                final TextView favRestView = new TextView(getContext());
                favRestView.setText("Favorite restaurant:");
                layout.addView(favRestView);
                final EditText favRestInputView = new EditText(getContext());
                favRestInputView.setFilters(new InputFilter[]{new InputFilter.LengthFilter(30)});
                favRestInputView.setInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
                favRestInputView.setText(favRestTextView.getText());
                layout.addView(favRestInputView);

                final TextView favCuisView = new TextView(getContext());
                favCuisView.setText("Favorite cuisine:");
                layout.addView(favCuisView);
                final EditText favCuisInputView = new EditText(getContext());
                favCuisInputView.setFilters(new InputFilter[]{new InputFilter.LengthFilter(30)});
                favCuisInputView.setInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
                favCuisInputView.setText(favCuisTextView.getText());
                layout.addView(favCuisInputView);

                builder.setView(layout);
                // add buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String newName = nameInputView.getText().toString();
                        String newGender = genderInputView.getText().toString();
                        String newFavRest = favRestInputView.getText().toString();
                        String newFavCuis = favCuisInputView.getText().toString();
                        DatabaseReference myRef = Db.getReference("Users");
                        FirebaseUser user = mAuth.getCurrentUser();
                        myRef.child(user.getUid()).child("name").setValue(newName);
                        myRef.child(user.getUid()).child("gender").setValue(newGender);
                        myRef.child(user.getUid()).child("favRest").setValue(newFavRest);
                        myRef.child(user.getUid()).child("favCuis").setValue(newFavCuis);
                        loadAllTextViews();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });
    }
}