package com.example.yorickbolster.eatr.RoomDatabase;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class MyContacts {
    @PrimaryKey @NonNull
    private String uid;

    @ColumnInfo(name = "favCuis")
    private String favCuis;

    @ColumnInfo(name = "favRest")
    private String favRest;

    @ColumnInfo(name = "gender")
    private String gender;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "picChangedAmount")
    private String picChangedAmount;


    @ColumnInfo(name = "timeLastChanged")
    private String timeLastChanged;


    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getFavCuis() {
        return favCuis;
    }

    public void setFavCuis(String favCuis) {
        this.favCuis = favCuis;
    }

    public String getFavRest() {
        return favRest;
    }

    public void setFavRest(String favRest) {
        this.favRest = favRest;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicChangedAmount() {
        return picChangedAmount;
    }

    public void setPicChangedAmount(String picChangedAmount) {
        this.picChangedAmount = picChangedAmount;
    }

    public String getTimeLastChanged() {
        return timeLastChanged;
    }

    public void setTimeLastChanged(String timeLastChanged) {
        this.timeLastChanged = timeLastChanged;
    }

    public MyContacts(String uid, String favCuis, String favRest, String gender, String name, String picChangedAmount, String timeLastChanged) {
        this.uid = uid;
        this.favCuis = favCuis;
        this.favRest = favRest;
        this.gender = gender;
        this.name = name;
        this.picChangedAmount = picChangedAmount;
        this.timeLastChanged = timeLastChanged;
    }

}