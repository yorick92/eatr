package com.example.yorickbolster.eatr;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class MyIntentService extends IntentService {


    public MyIntentService() {
        super("MyIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // TODO Auto-generated method stub
        Log.e("tag", "ayyy2");

        ResultReceiver rec = intent.getParcelableExtra("receiverTag");
        String recName= intent.getStringExtra("nameTag");
        Log.e("sohail","received name="+recName);

        Log.e("sohail","sending data back to activity");

        Bundle b= new Bundle();
        b.putString("ServiceTag","aziz");
        rec.send(0, b);
    }

}
