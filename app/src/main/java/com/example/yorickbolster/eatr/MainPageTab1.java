package com.example.yorickbolster.eatr;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.yorickbolster.eatr.DatabaseHelpers.ContactsDatabaseHelpers;
import com.example.yorickbolster.eatr.DatabaseHelpers.EventsDatabaseHelpers;
import com.example.yorickbolster.eatr.ImageTransformations.BlurTransformation;
import com.example.yorickbolster.eatr.ImageTransformations.BrightnessFilterTransformation;
import com.example.yorickbolster.eatr.Interfaces.RebuildListInterface;
import com.example.yorickbolster.eatr.RoomDatabase.Event;
import com.example.yorickbolster.eatr.RoomDatabase.MyContacts;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;


/**
 * RVFragment - Fragment with a simple RecyclerView that
 * only takes Strings
 * <p>
 * Usage:
 * RVFragment rvf = new RVFragment();
 *
 * @author Sheharyar Naseer
 */


public class MainPageTab1 extends Fragment implements RebuildListInterface {

    int[] first3JoinedPeople = {R.drawable.yorick, R.drawable.hussain, R.drawable.papito};

    List<Event> listOfEvents = null;

    Context appContext;

    DateFormat fullDateFormat;
    RecyclerView rv;
    private FirebaseAuth mAuth;
    DatabaseReference databaseEvents;
    private StorageReference mStorageRef;
    private EventsDatabaseHelpers eventsDatabaseHelpers;

    FirebaseUser currentUser;

    @Override
    public void rebuildList() {

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                try {
                    List<Event> EventsList = eventsDatabaseHelpers.getGetEvents().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR).get();

                    for(Event localEvent : EventsList){
                        listOfEvents.add(localEvent);
                    }

                    LinearLayoutManager layoutManager = new LinearLayoutManager(appContext);
                    rv.setLayoutManager(layoutManager);
                    RecyclerViewAdapter adapter = new RecyclerViewAdapter(listOfEvents);
                    rv.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    // taaadaaa
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        appContext = getActivity();
        eventsDatabaseHelpers = new EventsDatabaseHelpers(appContext);

        rv = new RecyclerView(getContext());
        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        mStorageRef = FirebaseStorage.getInstance().getReference();
        ((MainPageTabbed) getActivity()).setFabScrollbehaviour(rv); // scrollbehaviour FAB
        fullDateFormat = new SimpleDateFormat("EE d MMMM, HH:mm");

        return rv;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView locationView, priceRangeView, dateView, peopleCounter, creatorView;
        public ImageView locationImage, creatorImageView, joinedPerson3, joinedPerson2, joinedPerson1;
        public FrameLayout peopleCounterView;

        public MyViewHolder(View v) {
            super(v);
            locationImage = v.findViewById(R.id.locationPic);
            creatorImageView = v.findViewById(R.id.creatorImageView2);
            locationView = v.findViewById(R.id.locationView);
            priceRangeView = v.findViewById(R.id.priceRangeView);
            dateView = v.findViewById(R.id.dateView);
            creatorView = v.findViewById(R.id.creatorView);

            peopleCounter = v.findViewById(R.id.joinedPeopleCounter);
            peopleCounterView = v.findViewById(R.id.joinedPersonMore);
            joinedPerson1 = v.findViewById(R.id.joinedPerson1);
            joinedPerson2 = v.findViewById(R.id.joinedPerson2);
            joinedPerson3 = v.findViewById(R.id.joinedPerson3);
        }
    }

    /**
     * A Simple Adapter for the RecyclerView
     */

    public class RecyclerViewAdapter extends RecyclerView.Adapter<MyViewHolder> {
        private List<Event> dataSource;

        public RecyclerViewAdapter(List<Event> dataArgs) {
            dataSource = dataArgs;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_layout_event, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            final Event localEvent = dataSource.get(position);

            holder.locationView.setText(localEvent.getLocationName());

            holder.dateView.setText(fullDateFormat.format(new Date(Long.parseLong(localEvent.getMilliseconds()))));
            holder.creatorView.setText(localEvent.getCreator().getName());


            if (localEvent.getPriceBegin().equals("-1")) {
                holder.priceRangeView.setText("No price estimate");
            } else {
                holder.priceRangeView.setText("€" + localEvent.getPriceBegin() + " - " + dataSource.get(position).getPriceEnd());
            }

            holder.peopleCounter.setText("+" + String.valueOf(localEvent.getContacts().size()));

            // TODO get the images of 3 joined people
            holder.joinedPerson1.setImageResource(first3JoinedPeople[0]);
            holder.joinedPerson2.setImageResource(first3JoinedPeople[1]);
            holder.joinedPerson3.setImageResource(first3JoinedPeople[2]);
//            holder.joinedPerson3.setImageBitmap(dataSource.get(position).getBitmapPerson3());


            mStorageRef.child("pics/" + "Event/" + localEvent.getId() + "/eventPic.jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Picasso.with(appContext).load(uri).transform(new BlurTransformation(appContext)).transform(new BrightnessFilterTransformation(appContext, -0.3f)).into(holder.locationImage);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e("tab1 mStorageRef", "event has no GMaps picture");
                }
            });

            mStorageRef.child("pics/" + "Users/" + localEvent.getCreator() + "/profile.jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Picasso.with(appContext).load(uri).into(holder.creatorImageView);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e("tab1 mStorageRef", "creator has no profile picture");
                }
            });


            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent goToListItem = new Intent(getActivity(), OpenEvent.class);
                    goToListItem.putExtra("eventId", localEvent.getId());
                    goToListItem.putExtra("creatorId", localEvent.getCreatorId());
                    startActivity(goToListItem);
                }
            });
        }

        @Override
        public int getItemCount() {
            return dataSource.size();
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        mAuth = FirebaseAuth.getInstance();
        databaseEvents = FirebaseDatabase.getInstance().getReference("Events");
        listOfEvents = new ArrayList<>();

        currentUser = mAuth.getCurrentUser();
        if (currentUser == null) {
            updateUI();
        }
    }


    private void updateUI() {
        Toast.makeText(getActivity(), "you are logged out", Toast.LENGTH_LONG).show();

        Intent goToMainActivity = new Intent(getActivity(), MainActivity.class);
        startActivity(goToMainActivity);
    }

}

