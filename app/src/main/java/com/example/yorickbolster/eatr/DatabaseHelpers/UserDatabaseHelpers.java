package com.example.yorickbolster.eatr.DatabaseHelpers;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.yorickbolster.eatr.HelperClasses.DispatchGroup;
import com.example.yorickbolster.eatr.RoomDatabase.AppDatabase;
import com.example.yorickbolster.eatr.RoomDatabase.MyContacts;
import com.example.yorickbolster.eatr.RoomDatabase.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

/**
 * Created by yorickbolster on 19/07/2018.
 */

public class UserDatabaseHelpers {
    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference refUser;
    private DispatchGroup DispatchContacts;
    AppDatabase dbUser;

    public UserDatabaseHelpers(Context context){
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        firebaseDatabase = FirebaseDatabase.getInstance();
        refUser = firebaseDatabase.getReference("Users").child(currentUser.getUid());

        dbUser = Room.databaseBuilder(context, AppDatabase.class, "user").build();
    }

    public void syncUserData(DispatchGroup DispatchContactPassed) {
        DispatchContacts = DispatchContactPassed;
        refUser.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("dataSnapshot.getKey()", dataSnapshot.getKey());
                User user = new User(
                        String.valueOf(dataSnapshot.getKey()),
                        String.valueOf(dataSnapshot.child("email").getValue()),
                        String.valueOf(dataSnapshot.child("facebookFriends").getValue()),
                        String.valueOf(dataSnapshot.child("facebookId").getValue()),
                        String.valueOf(dataSnapshot.child("facebookPicUrl").getValue()),
                        String.valueOf(dataSnapshot.child("favCuis").getValue()),
                        String.valueOf(dataSnapshot.child("favRest").getValue()),
                        String.valueOf(dataSnapshot.child("gender").getValue()),
                        String.valueOf(dataSnapshot.child("groups").getValue()),
                        String.valueOf(dataSnapshot.child("joinedEvents").getValue()),
                        String.valueOf(dataSnapshot.child("myEvents").getValue()),
                        String.valueOf(dataSnapshot.child("name").getValue()),
                        String.valueOf(dataSnapshot.child("picChangedAmount").getValue()),
                        String.valueOf(dataSnapshot.child("presets").getValue()),
                        String.valueOf(dataSnapshot.child("timeLastChanged").getValue())
                );
                new RoomSaveUser().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, user);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("FireDatabaseDebug", "UserDatabaseHelpers setting valueEventListener failed");
                DispatchContacts.leave();
            }
        });
    }

    private class RoomSaveUser extends AsyncTask<User, Void, Void> {
        @Override
        protected Void doInBackground(User... users) {

            if (users.length > 0) {
                dbUser.userDao().insertAll(users[0]);
                DispatchContacts.leave();
            }
            else {
                Log.d("RoomDatabaseDebug", "UserDatabaseHelpers you passed an empty list of users");
                DispatchContacts.leave();
            }
            return null;
        }
    }

    public RoomGetUser getRoomGetUser(){
        return new RoomGetUser();
    }

    public class RoomGetUser extends AsyncTask<Void, Void, User> {
        @Override
        public User doInBackground(Void... voids) {
            List<User> aList = dbUser.userDao().getAll();
            if(aList.size() > 0){
                if(aList.size() > 1){
                    Log.d("RoomDatabaseDebug", "UserDatabaseHelpers you have more than 1 user saved in your Room database: " + aList );
                }
                return aList.get(0);
            }
            else{
                return null;
            }
        }
    }

    public RoomDeleteUser getRoomDeleteUser(){
        return new RoomDeleteUser();
    }

    public class RoomDeleteUser extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            dbUser.userDao().deleteAll();
            return null;
        }
    }
}
