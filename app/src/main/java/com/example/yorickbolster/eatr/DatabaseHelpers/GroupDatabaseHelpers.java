package com.example.yorickbolster.eatr.DatabaseHelpers;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.yorickbolster.eatr.HelperClasses.DispatchGroup;
import com.example.yorickbolster.eatr.RoomDatabase.AllContacts;
import com.example.yorickbolster.eatr.RoomDatabase.AppDatabase;
import com.example.yorickbolster.eatr.RoomDatabase.AppGroup;
import com.example.yorickbolster.eatr.RoomDatabase.Event;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yorickbolster on 21/07/2018.
 */

public class GroupDatabaseHelpers {

    private DatabaseReference refUser;
    private DatabaseReference refGroups;
    private DatabaseReference refPublicProfiles;
    private DispatchGroup DispatchGroups;
    private DispatchGroup GotAllGroups;

    private AppDatabase dbAllContacts, dbAppGroup;

    private List<AppGroup> AllAppGroups;

    private ContactsDatabaseHelpers contactsDatabaseHelpers;

    boolean startUp = true;

    public GroupDatabaseHelpers(Context context) {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();

        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        refUser = firebaseDatabase.getReference("Users").child(currentUser.getUid());
        refGroups = firebaseDatabase.getReference("groups");
        refPublicProfiles = firebaseDatabase.getReference("publicProfiles");
        dbAppGroup = Room.databaseBuilder(context, AppDatabase.class, "AppGroup").build();
        dbAllContacts = Room.databaseBuilder(context, AppDatabase.class, "allcontacts").build();

        contactsDatabaseHelpers = new ContactsDatabaseHelpers(context);
    }

    private boolean ContactNeedsToUpdate(String theId, String timeLastChanged){
        try{
            AllContacts theContact = contactsDatabaseHelpers.getRoomGetAllContactById().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, theId).get();
            Log.d("DebugAllContacts", "ContactNeedsToUpdate: " + theContact);
            if(theContact != null){
                if(theContact.getTimeLastChanged().equals(timeLastChanged)){
                    return false;
                }
                else{
                    return true;
                }
            }
            else{
                return true;
            }
        }
        catch(Exception e){
            Log.e("Exception", "checkIfContactExists: " + e);
            return false;
        }

    }

    private void loadAllContactFromDatabase(String theId, final DispatchGroup localDispatchGroup){ // this function is a double, also in EventsDatabaseHelpers
        refPublicProfiles.child(theId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                AllContacts allContact = new AllContacts(
                        dataSnapshot.getKey(),
                        String.valueOf(dataSnapshot.child("favCuis").getValue()),
                        String.valueOf(dataSnapshot.child("favRest").getValue()),
                        String.valueOf(dataSnapshot.child("gender").getValue()),
                        String.valueOf(dataSnapshot.child("name").getValue()),
                        String.valueOf(dataSnapshot.child("picChangedAmount").getValue()),
                        String.valueOf(dataSnapshot.child("timeLastChanged").getValue())
                );

                try{
                    Log.d("DebugAllContacts", "onDataChange: calling save to roomdatabase for new allcontacts: " + allContact.getName());
                    Void a = contactsDatabaseHelpers.getRoomSaveAllContacts().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, allContact).get(); // getting void so it waits for execution
                }
                catch (Exception e){

                }
                localDispatchGroup.leave();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                localDispatchGroup.leave();
            }
        });
    }

    private  void loadGroupFromDatabase(DatabaseReference refGroups, String groupId){
        final DispatchGroup localDispatchGroup = new DispatchGroup("loadGroupFromDatabase");
        localDispatchGroup.enter();
        refGroups.child(groupId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot3) {

                ArrayList<String> contactIds = new ArrayList<>();
                for (DataSnapshot idSS : dataSnapshot3.child("contacts").getChildren()) {
                    final String localId = String.valueOf(idSS.getKey());
                    contactIds.add(localId);

                    refPublicProfiles.child(localId).child("timeLastChanged").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot4) {
                            if(ContactNeedsToUpdate(localId, String.valueOf(dataSnapshot4.getValue()))){
                                loadAllContactFromDatabase(localId, localDispatchGroup);
                            }
                            else{
                                localDispatchGroup.leave();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            localDispatchGroup.leave();
                        }
                    });
                }

                AppGroup localAppGroup = new AppGroup(
                        dataSnapshot3.getKey(),
                        String.valueOf(dataSnapshot3.child("name").getValue()),
                        String.valueOf(dataSnapshot3.child("admin").getValue()),
                        String.valueOf(dataSnapshot3.child("timeLastChanged").getValue()),
                        contactIds
                );

                if(startUp){
                    AllAppGroups.add(localAppGroup);
                    GotAllGroups.leave();
                }
                else{
                    new RoomSaveGroups().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, localAppGroup);
                }
                localDispatchGroup.leave();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                GotAllGroups.leave();
            }
        });
    }

    private boolean groupInfoChanged(String id, String timeLastChanged){
        try{
            AppGroup aGroup = new RoomGetGroupById().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,id).get();
            if(aGroup != null){
                if(aGroup.getTimeLastChanged().equals(timeLastChanged)){
                    return false;
                }
                else{
                    return true;
                }
            }
            else{
                return true;
            }
        }
        catch(Exception e){
            Log.e("Exception", "contactInfoChangedException: " + e);
            return  false;
        }
    }

    public void syncAllGroupData(DispatchGroup passedDispatchGroup) {
        DispatchGroups = passedDispatchGroup;
        refUser.child("groups").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                GotAllGroups = new DispatchGroup("GotAllGroups");
                AllAppGroups = new ArrayList<>();

                for (DataSnapshot friendSnapshot : dataSnapshot.getChildren()) {
                    GotAllGroups.enter();

                    final String groupId = friendSnapshot.getKey();
                    refGroups.child(groupId).child("timeLastChanged").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot2) {
                            String timeLastChanged = dataSnapshot2.getValue().toString();
                            if(groupInfoChanged(groupId, timeLastChanged)){
                                Log.d("FireDatabaseDebug", "group " + groupId + " changed");
                                loadGroupFromDatabase(refGroups, groupId);
                            }
                            else{
                                Log.d("FireDatabaseDebug", "group " + groupId + " unchanged");
                                GotAllGroups.leave();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }

                final Runnable saveGroups = new Runnable() {
                    @Override
                    public void run() {
                        new RoomSaveGroups().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AllAppGroups.toArray(new AppGroup[AllAppGroups.size()]));
                    }
                };

                GotAllGroups.notify(saveGroups);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                DispatchGroups.leave();
            }
        });
    }

    public class RoomSaveGroups extends AsyncTask<AppGroup, Void, Void> {

        @Override
        protected Void doInBackground(AppGroup... groups) {
            if (groups.length > 0) {
                dbAppGroup.groupsDao().insertAll(groups);
                if(startUp){
                    DispatchGroups.leave();
                }
                startUp = false;
            } else {
                DispatchGroups.leave();
            }
            return null;
        }
    }

    private AppGroup addContactObjects(AppGroup theGroup){
        List<String> localContactIdsList = theGroup.getContactIds();
        String[] localContactIdsArray = localContactIdsList.toArray(new String[localContactIdsList.size()]);
        List<AllContacts> contacts = dbAllContacts.allContactsDao().loadByIds(localContactIdsArray);
        theGroup.setContacts(contacts);

        String creatorId = theGroup.getAdminId();
        List<AllContacts> creatorList = dbAllContacts.allContactsDao().loadById(creatorId);
        if(creatorList.size() > 0){
            theGroup.setAdmin(creatorList.get(0));
        }
        return theGroup;
    }

    RoomGetGroupById getRoomGetGroupById(){
        return new RoomGetGroupById();
    }

    public class RoomGetGroupById extends AsyncTask<String, Void, AppGroup>{

        @Override
        protected AppGroup doInBackground(String... strings) {
            if(strings.length < 1){
                return null;
            }
            List<AppGroup> localGroupList = dbAppGroup.groupsDao().loadById(strings[0]);
            if(localGroupList.size() > 0){
                return addContactObjects(localGroupList.get(0));
            }
            else{
                return null;
            }
        }
    }

    RoomGetGroupsByIds getRoomGetGroupsByIds(){
        return  new RoomGetGroupsByIds();
    }

    public class RoomGetGroupsByIds extends AsyncTask<String, Void, List<AppGroup>>{

        @Override
        protected List<AppGroup> doInBackground(String... strings) {
            List<AppGroup> localGroupList = dbAppGroup.groupsDao().loadByIds(strings);
            for(int i = 0; i < localGroupList.size(); i++){
                localGroupList.set(i, addContactObjects(localGroupList.get(i)));
            }
            return localGroupList;
        }
    }

    RoomGetGroups getRoomGetGroups(){
        return  new RoomGetGroups();
    }

    public class RoomGetGroups extends AsyncTask<Void, Void, List<AppGroup>>{

        @Override
        protected List<AppGroup> doInBackground(Void... voids) {
            List<AppGroup> localGroupList = dbAppGroup.groupsDao().getAll();
            for(int i = 0; i < localGroupList.size(); i++){
                localGroupList.set(i, addContactObjects(localGroupList.get(i)));
            }
            return localGroupList;
        }
    }

    public RoomDeleteAllGroups getRoomDeleteAllGroups(){
        return new RoomDeleteAllGroups();
    }

    public class RoomDeleteAllGroups extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            dbAppGroup.groupsDao().deleteAll();
            return null;
        }
    }

}