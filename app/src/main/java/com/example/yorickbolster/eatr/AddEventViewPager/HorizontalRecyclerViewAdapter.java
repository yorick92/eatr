package com.example.yorickbolster.eatr.AddEventViewPager;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.yorickbolster.eatr.ImageTransformations.BlurTransformation;
import com.example.yorickbolster.eatr.ImageTransformations.BrightnessFilterTransformation;
import com.example.yorickbolster.eatr.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class HorizontalRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Object> dataSource = new ArrayList();
    private final static int TYPE_PLACE = 1, TYPE_CUSTOM = 2;

    private Context context;

    // Constructor
    public HorizontalRecyclerViewAdapter(Context context){
        this.context=context;
    }

    public void setDataSource(List<Object> dataSource){
        this.dataSource = dataSource;
    }

    @Override
    public int getItemViewType(int position) {
        if (dataSource.get(position) instanceof Places) {
            return TYPE_PLACE;
        } else if (dataSource.get(position) instanceof SearchPlace) {
            return TYPE_CUSTOM;
        }
        return -1;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        int viewType=holder.getItemViewType();
        switch (viewType){
            case TYPE_CUSTOM:
                SearchPlace search =(SearchPlace) dataSource.get(position);
                ((SearchPlaceViewHolder)holder).populateCard(search);
                break;
            case TYPE_PLACE:
                Places place=(Places) dataSource.get(position);
                ((PlaceViewHolder)holder).populateCard(place);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return dataSource.size();
    }

    // Invoked by layout manager to create new views
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Attach layout for single cell
        int layout = 0;
        RecyclerView.ViewHolder viewHolder;
        // Identify viewType returned by getItemViewType(...)
        // and return ViewHolder Accordingly
        switch (viewType){
            case TYPE_CUSTOM:
                layout= R.layout.listitem_add_event_search;
                View callsView = LayoutInflater
                        .from(parent.getContext())
                        .inflate(layout, parent, false);
                viewHolder=new SearchPlaceViewHolder(callsView);
                break;
            case TYPE_PLACE:
                layout=R.layout.listitem_add_event_place;
                View smsView = LayoutInflater
                        .from(parent.getContext())
                        .inflate(layout, parent, false);
                viewHolder=new PlaceViewHolder(smsView);
                break;
            default:
                viewHolder=null;
                break;
        }
        return viewHolder;
    }

    // First ViewHolder of object type Call
    // Reference to the views for each call items to display desired information
    public class SearchPlaceViewHolder extends RecyclerView.ViewHolder {

        private TextView callerNameTextView,callTimeTextView;
        private ImageView searchImageView;

        public SearchPlaceViewHolder(View itemView) {
            super(itemView);
            // Initiate view
//            callerNameTextView = itemView.findViewById(R.id.callerName);
//            callTimeTextView = itemView.findViewById(R.id.callTime);
            searchImageView = itemView.findViewById(R.id.searchImage);
        }

        public void populateCard(SearchPlace search){
            // Attach values for each item
//            String callerName   = search.getCallerName();
//            String callTime     = search.getCallTime();
//            Bitmap image        = search.getImage();
//

//            Picasso.with(context).load(image).transform(new BlurTransformation(context)).transform(new BrightnessFilterTransformation(context, -0.3f)).into(searchImageView);

//            callerNameTextView.setText(callerName);
//            callTimeTextView.setText(callTime);
//            searchImageView.setImageBitmap(image);
        }
    }

    // Second ViewHolder of object type SMS
    // Reference to the views for each call items to display desired information
    public class PlaceViewHolder extends RecyclerView.ViewHolder {

        private TextView senderNameTextView,smsContentTextView, smsTimeTextView;
        RatingBar ratingBar;

        public PlaceViewHolder(View itemView) {
            super(itemView);
            // Initiate view
//            senderNameTextView  = itemView.findViewById(R.id.senderName);
//            smsContentTextView  = itemView.findViewById(R.id.smsContent);
//            smsTimeTextView     = itemView.findViewById(R.id.smsTime);
            ratingBar = itemView.findViewById(R.id.placeRatingBar);
        }

        public void populateCard(Places place){
            // Attach values for each item
//            String senderName   = place.getSenderName();
//            String smsContent   = place.getSmsContent();
//            String smsTime      = place.getSmsTime();
            ratingBar.setRating(4.2f);
//            senderNameTextView.setText(senderName);
//            smsContentTextView.setText(smsContent);
//            smsTimeTextView.setText(smsTime);
        }
    }
}
