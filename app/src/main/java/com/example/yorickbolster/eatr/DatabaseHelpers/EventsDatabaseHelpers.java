package com.example.yorickbolster.eatr.DatabaseHelpers;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.yorickbolster.eatr.HelperClasses.DispatchGroup;
import com.example.yorickbolster.eatr.RoomDatabase.AllContacts;
import com.example.yorickbolster.eatr.RoomDatabase.AppDatabase;
import com.example.yorickbolster.eatr.RoomDatabase.Event;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yorickbolster on 25/07/2018.
 */

public class EventsDatabaseHelpers {
    FirebaseUser currentUser;
    private DatabaseReference refUser;
    private DatabaseReference refEvents;
    private DatabaseReference refEventInvitees;
    private DatabaseReference refPublicProfiles;
    private DispatchGroup DispatchEvents;
    private DispatchGroup GotAllEvents;

    private AppDatabase dbAllContacts, dbEvents;

    private List<Event> allEvents;
    private List<String> eventIds;
    private boolean startUp;

    private ContactsDatabaseHelpers contactsDatabaseHelpers;


    public EventsDatabaseHelpers(Context context){
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();

        refUser = firebaseDatabase.getReference("Users").child(currentUser.getUid());
        refEvents = firebaseDatabase.getReference("Events");
        refEventInvitees = firebaseDatabase.getReference("EventInvitees");
        refPublicProfiles = firebaseDatabase.getReference("publicProfiles");
        dbEvents = Room.databaseBuilder(context, AppDatabase.class, "Event").build();
        dbAllContacts = Room.databaseBuilder(context, AppDatabase.class, "allcontacts").build();
        startUp = true;

        contactsDatabaseHelpers = new ContactsDatabaseHelpers(context);
    }

    private boolean ContactNeedsToUpdate(String theId, String timeLastChanged){
        try{
            AllContacts theContact = contactsDatabaseHelpers.getRoomGetAllContactById().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, theId).get();
            Log.d("DebugAllContacts", "ContactNeedsToUpdate: " + theContact);
            if(theContact != null){
                if(theContact.getTimeLastChanged().equals(timeLastChanged)){
                    return false;
                }
                else{
                    return true;
                }
            }
            else{
                return true;
            }
        }
        catch(Exception e){
            Log.e("Exception", "checkIfContactExists: " + e);
            return false;
        }

    }

    private void loadAllContactFromDatabase(String theId){  // this function is a double, also in GroupDatabaseHelpers
        refPublicProfiles.child(theId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                AllContacts allContact = new AllContacts(
                        dataSnapshot.getKey(),
                        String.valueOf(dataSnapshot.child("favCuis").getValue()),
                        String.valueOf(dataSnapshot.child("favRest").getValue()),
                        String.valueOf(dataSnapshot.child("gender").getValue()),
                        String.valueOf(dataSnapshot.child("name").getValue()),
                        String.valueOf(dataSnapshot.child("picChangedAmount").getValue()),
                        String.valueOf(dataSnapshot.child("timeLastChanged").getValue())
                );

                try{
                    Log.d("DebugAllContacts", "onDataChange: calling save to roomdatabase for new allcontacts: " + allContact.getName());
                    Void a = contactsDatabaseHelpers.getRoomSaveAllContacts().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, allContact).get(); // getting void so it waits for execution
                }
                catch (Exception e){

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private  void loadEventFromDatabase(String eventId){
        refEvents.child(eventId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot3) {

                ArrayList<String> contactIds = new ArrayList<>();
                for (DataSnapshot idSS : dataSnapshot3.child("contacts").getChildren()) {
                    final String localId = String.valueOf(idSS.getKey());
                    contactIds.add(localId);

                    refPublicProfiles.child(localId).child("timeLastChanged").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot4) {
                            if(ContactNeedsToUpdate(localId, String.valueOf(dataSnapshot4.getValue()))){
                                loadAllContactFromDatabase(localId);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });
                }

                Event localEvent = new Event(
                        dataSnapshot3.getKey(),
                        String.valueOf(dataSnapshot3.child("creator").getValue()),
                        contactIds,
                        String.valueOf(dataSnapshot3.child("groupOrNot").getValue()),
                        String.valueOf(dataSnapshot3.child("latitude").getValue()),
                        String.valueOf(dataSnapshot3.child("locationAddress").getValue()),
                        String.valueOf(dataSnapshot3.child("locationId").getValue()),
                        String.valueOf(dataSnapshot3.child("locationName").getValue()),
                        String.valueOf(dataSnapshot3.child("longitude").getValue()),
                        String.valueOf(dataSnapshot3.child("maxPeople").getValue()),
                        String.valueOf(dataSnapshot3.child("miliseconds").getValue()),
                        String.valueOf(dataSnapshot3.child("name").getValue()),
                        String.valueOf(dataSnapshot3.child("picChangedAmount").getValue()),
                        String.valueOf(dataSnapshot3.child("priceBegin").getValue()),
                        String.valueOf(dataSnapshot3.child("priceEnd").getValue()),
                        String.valueOf(dataSnapshot3.child("remarks").getValue()),
                        String.valueOf(dataSnapshot3.child("timeLastChanged").getValue()),
                        String.valueOf(dataSnapshot3.child("timeMade").getValue()),
                        String.valueOf(dataSnapshot3.child("typeOfEvent").getValue())
                );

                if(startUp){
                    allEvents.add(localEvent);
                    GotAllEvents.leave();
                }
                else{
                    new SaveEvents().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, localEvent);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                GotAllEvents.leave();
            }
        });
    }

    private boolean eventInfoChanged(String id, String timeLastChanged){
        try{
            Event anEvent = new GetEventById().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, id).get();
            if(anEvent != null){
                if(anEvent.getTimeLastChanged().equals(timeLastChanged)){
                    return false;
                }
                else{
                    return true;
                }
            }
            else{
                return true;
            }
        }
        catch(Exception e){
            Log.e("Exception", "contactInfoChangedException: " + e);
            return  false;
        }
    }

    public void syncAllEventData(DispatchGroup passedDispatchGroup) {
        DispatchEvents = passedDispatchGroup;
        refEventInvitees.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                eventIds = new ArrayList<>();
                for(DataSnapshot eventInviteesSS :dataSnapshot.getChildren()){
                    if(eventInviteesSS.hasChild(currentUser.getUid())){
                        eventIds.add(eventInviteesSS.getKey());
                    }
                }

                GotAllEvents = new DispatchGroup("GotAllEvents");
                allEvents = new ArrayList<>();

                for(final String eventId : eventIds){
                    GotAllEvents.enter();
                    refEvents.child(eventId).child("timeLastChanged").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot2) {
                            if(eventInfoChanged(eventId, String.valueOf(dataSnapshot2.getValue()))){
                                loadEventFromDatabase(eventId);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            GotAllEvents.leave();
                        }
                    });
                }

                final Runnable saveEvents = new Runnable() {
                    @Override
                    public void run() {
                        new SaveEvents().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, allEvents.toArray(new Event[allEvents.size()]));
                    }
                };

                GotAllEvents.notify(saveEvents);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                DispatchEvents.leave();
            }
        });
    }

    public class SaveEvents extends AsyncTask<Event, Void, Void> {

        @Override
        protected Void doInBackground(Event... events) {
            if (events.length > 0) {
                dbEvents.eventDao().insertAll(events);
                if(startUp){
                    DispatchEvents.leave();
                }
                startUp = false;
            } else {
                DispatchEvents.leave();
            }
            return null;
        }
    }

    private Event addContactObjects(Event theEvent){
        List<String> localContactIdsList = theEvent.getContactIds();
        String[] localContactIdsArray = localContactIdsList.toArray(new String[localContactIdsList.size()]);
        List<AllContacts> contacts = dbAllContacts.allContactsDao().loadByIds(localContactIdsArray);
        theEvent.setContacts(contacts);

        String creatorId = theEvent.getCreatorId();
        List<AllContacts> creatorList = dbAllContacts.allContactsDao().loadById(creatorId);
        if(creatorList.size() > 0){
            theEvent.setCreator(creatorList.get(0));
        }
        return theEvent;
    }

    public GetEventById getGetEventById(){
        return new GetEventById();
    }

    public class GetEventById extends AsyncTask<String, Void, Event>{

        @Override
        protected Event doInBackground(String... strings) {
            if(strings.length < 1){
                return null;
            }
            List<Event> localEventList = dbEvents.eventDao().loadById(strings[0]);
            if(localEventList.size() > 0){
                return addContactObjects(localEventList.get(0));
            }
            else{
                return null;
            }
        }
    }

    public GetEventsByIds getGetEventsByIds(){
        return new GetEventsByIds();
    }

    public class GetEventsByIds extends AsyncTask<String, Void, List<Event>>{

        @Override
        protected List<Event> doInBackground(String... strings) {
            List<Event> localEventList = dbEvents.eventDao().loadByIds(strings);
            for(int i = 0; i < localEventList.size(); i++){
                localEventList.set(i, addContactObjects(localEventList.get(i)));
            }
            return localEventList;
        }
    }

    public GetEvents getGetEvents(){
        return new GetEvents();
    }

    public class GetEvents extends AsyncTask<Void, Void, List<Event>>{

        @Override
        protected List<Event> doInBackground(Void... voids) {
            List<Event> localEventList = dbEvents.eventDao().getAll();
            for(int i = 0; i < localEventList.size(); i++){
                localEventList.set(i, addContactObjects(localEventList.get(i)));
            }
            return localEventList;
        }
    }

    public DeleteAllEvents getDeleteAllEvents(){
        return new DeleteAllEvents();
    }

    public class DeleteAllEvents extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            dbEvents.eventDao().deleteAll();
            return null;
        }
    }
}
