package com.example.yorickbolster.eatr.DatabaseHelpers;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.yorickbolster.eatr.RoomDatabase.AllContacts;
import com.example.yorickbolster.eatr.RoomDatabase.AppGroup;
import com.example.yorickbolster.eatr.RoomDatabase.Event;
import com.example.yorickbolster.eatr.RoomDatabase.MyContacts;
import com.example.yorickbolster.eatr.RoomDatabase.Preset;
import com.example.yorickbolster.eatr.RoomDatabase.User;

import java.util.List;

/**
 * Created by yorickbolster on 21/07/2018.
 */

public class RoomDataTesters {

    private UserDatabaseHelpers userDatabaseHelpers;
    private ContactsDatabaseHelpers contactsDatabaseHelpers;
    private PresetDatabaseHelpers presetDatabaseHelpers;
    private GroupDatabaseHelpers groupDatabaseHelpers;
    private EventsDatabaseHelpers eventsDatabaseHelpers;

    public final Runnable testAll;
    private final Runnable testUser;
    private final Runnable testMyContacts;
    private final Runnable testPresets;
    private final Runnable testGroups;
    private final Runnable testEvents;
    private final Runnable testAllContacts;

    public RoomDataTesters(Context context){
        userDatabaseHelpers = new UserDatabaseHelpers(context);
        contactsDatabaseHelpers = new ContactsDatabaseHelpers(context);
        presetDatabaseHelpers = new PresetDatabaseHelpers(context);
        groupDatabaseHelpers = new GroupDatabaseHelpers(context);
        eventsDatabaseHelpers = new EventsDatabaseHelpers(context);

        testUser = new Runnable() {
            @Override
            public void run() {
                try{
                    User theUser = userDatabaseHelpers.getRoomGetUser().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR).get();
                    Log.d("TestRoomData", "testUser: ");
                    if(theUser != null){
                        Log.d("TestRoomData", "testUser: " + theUser.getName());
                    }
                }
                catch(Exception e){
                    Log.e("RoomDatabaseExceptions", "An exception occured: " + e);
                }
            }
        };

        testMyContacts = new Runnable() {
            @Override
            public void run() {
                Log.d("TestRoomData", "testMyContacts: ");
                try{
                    List<MyContacts> test = contactsDatabaseHelpers.getRoomGetAllMyContacts().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR).get();
                    Log.d("TestRoomData", "testMyContacts: " + test);
                    for(int i = 0; i < test.size(); i++){
                        Log.d("TestRoomData", "testMyContacts: " + test.get(i).getName());
                    }
                }
                catch(Exception e){
                    Log.e("RoomDatabaseExceptions", "An exception occured: " + e);
                }
            }
        };

        testPresets = new Runnable() {
            @Override
            public void run() {
                Log.d("TestRoomData", "testPresets: ");
                try{
                    List<Preset> test = presetDatabaseHelpers.getRoomGetPresets().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR).get();
                    for(int i = 0; i < test.size(); i++){
                        Log.d("TestRoomData", "testPresets: " + test.get(i).getName());
                        for(int j = 0; j < test.get(i).getContacts().size(); j++){
                            Log.d("TestRoomData", "testPresetContacts: " + test.get(i).getContacts().get(j).getName());
                        }
                    }
                }
                catch(Exception e){
                    Log.e("RoomDatabaseExceptions", "An exception occured: " + e);
                }

            }
        };

        testGroups = new Runnable() {
            @Override
            public void run() {
                Log.d("TestRoomData", "testGroups: ");
                try{
                    List<AppGroup> test = groupDatabaseHelpers.getRoomGetGroups().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR).get();
                    for(int i = 0; i < test.size(); i++){
                        Log.d("TestRoomData", "testGroups: " + test.get(i).getName());
                        for(int j = 0; j < test.get(i).getContacts().size(); j++){
                            Log.d("TestRoomData", "testGroupContacts: " + test.get(i).getContacts().get(j).getName());
                        }
                    }
                }
                catch(Exception e){
                    Log.e("RoomDatabaseExceptions", "An exception occured: " + e);
                }
            }
        };

        testEvents = new Runnable() {
            @Override
            public void run() {
                Log.d("TestRoomData", "testEvents: ");
                try{
                    List<Event> test = eventsDatabaseHelpers.getGetEvents().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR).get();
                    for(int i = 0; i < test.size(); i++){
                        Log.d("TestRoomData", "testEvents: " + test.get(i).getName());
                        for(int j = 0; j < test.get(i).getContacts().size(); j++){
                            Log.d("TestRoomData", "testEventContacts: " + test.get(i).getContacts().get(j).getName());
                        }
                    }
                }
                catch(Exception e){
                    Log.e("RoomDatabaseExceptions", "An exception occured: " + e);
                }
            }
        };

        testAllContacts = new Runnable() {
            @Override
            public void run() {
                Log.d("TestRoomData", "testAllContacts: ");
                try{
                    List<AllContacts> test = contactsDatabaseHelpers.getRoomGetAllAllContacts().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR).get();
                    Log.d("TestRoomData", "testAllContacts: " + test);
                    for(int i = 0; i < test.size(); i++){
                        Log.d("TestRoomData", "testAllContacts: " + test.get(i).getName());
                    }
                }
                catch(Exception e){
                    Log.e("RoomDatabaseExceptions", "An exception occured: " + e);
                }
            }
        };

        testAll = new Runnable() {
            @Override
            public void run() {
                try{
                    testUser.run();
                    testMyContacts.run();
                    testPresets.run();
                    testGroups.run();
                    testAllContacts.run();
                    testEvents.run();
                }
                catch(Exception e){
                    Log.e("RoomDatabaseExceptions", "An exception occured: " + e);
                }
            }
        };
    }


}