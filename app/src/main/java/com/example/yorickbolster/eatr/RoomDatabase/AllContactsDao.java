package com.example.yorickbolster.eatr.RoomDatabase;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface AllContactsDao {

    @Query("SELECT * FROM AllContacts")
    List<AllContacts> getAll();

    @Query("SELECT * FROM AllContacts WHERE uid IN (:userIds)")
    List<AllContacts> loadByIds(String... userIds);

    @Query("SELECT * FROM AllContacts WHERE uid IN (:userId)")
    List<AllContacts> loadById(String userId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(AllContacts... allContacts);

    @Query("DELETE FROM AllContacts")
    void deleteAll();

    @Update
    void updateContact(AllContacts... allContacts);

    @Delete
    void delete(AllContacts allContacts);
}