package com.example.yorickbolster.eatr.AddEventViewPager;

import android.graphics.Bitmap;
import android.widget.ImageView;

public class Places {



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFeaturedTimestamp() {
        return featuredTimestamp;
    }

    public void setFeaturedTimestamp(String featuredTimestamp) {
        this.featuredTimestamp = featuredTimestamp;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

//    So yeah this is not allowed with the current Google API's.
//    public String getAmountReviews() {
//        return amountReviews;
//    }
//
//    public void setAmountReviews(String amountReviews) {
//        this.amountReviews = amountReviews;
//    }

    public String getTravelDistance() {
        return travelDistance;
    }

    public void setTravelDistance(String travelDistance) {
        this.travelDistance = travelDistance;
    }

    public String getPlaceType() {
        return placeType;
    }

    public void setPlaceType(String placeType) {
        this.placeType = placeType;
    }

    public String getAddressStreetNumber() {
        return addressStreetNumber;
    }

    public void setAddressStreetNumber(String addressStreetNumber) {
        this.addressStreetNumber = addressStreetNumber;
    }

    private String id;
    private String featuredTimestamp;
    private Bitmap image;
    private String name;
    private String rating;
    private String amountReviews;
    private String travelDistance;
    private String placeType;
    private String addressStreetNumber;
}
