package com.example.yorickbolster.eatr;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;

import java.util.Date;

public class EventItem implements Parcelable {
    private String eventId;
    private String creator;
    private String locationName;
    private String locationAddress;
    private String locationId;
    private Date dateStart;
    private String priceBegin;
    private String priceEnd;
    private String maxPeople;
    private Uri eventPicture;
    private Uri creatorPicture;
    private String joinedPeople;

    public EventItem(String eventId, String creator, String locationName, String locationAddress, String locationId, Date dateStart, String priceBegin, String priceEnd,
                     String maxPeople, String joinedPeople){
        this.eventId = eventId;
        this.creator = creator;
        this.locationName = locationName;
        this.locationAddress = locationAddress;
        this.locationId = locationId;
        this.dateStart = dateStart;
        this.priceBegin = priceBegin;
        this.priceEnd = priceEnd;
        this.maxPeople = maxPeople;
        this.joinedPeople = joinedPeople;
    }

    protected EventItem(Parcel in) {
        eventId = in.readString();
        creator = in.readString();
        locationName = in.readString();
        locationAddress = in.readString();
        locationId = in.readString();
        priceBegin = in.readString();
        priceEnd = in.readString();
        maxPeople = in.readString();
        eventPicture = in.readParcelable(Uri.class.getClassLoader());
        creatorPicture = in.readParcelable(Uri.class.getClassLoader());
        joinedPeople = in.readString();
    }

    public static final Creator<EventItem> CREATOR = new Creator<EventItem>() {
        @Override
        public EventItem createFromParcel(Parcel in) {
            return new EventItem(in);
        }

        @Override
        public EventItem[] newArray(int size) {
            return new EventItem[size];
        }
    };


    public void setEventPicture(Uri eventPicture) {
        this.eventPicture = eventPicture;
    }

    public void setCreatorPicture(Uri creatorPicture) {
        this.creatorPicture = creatorPicture;
    }


    public Uri getEventPicture() {
        return eventPicture;
    }

    public Uri getCreatorPicture() {
        return creatorPicture;
    }

    public String getJoinedPeople() {
        return joinedPeople;
    }

    public String getEventId() {
        return eventId;
    }

    public String getCreator() {
        return creator;
    }

    public String getPriceEnd() {
        return priceEnd;
    }

    public String getMaxPeople() {
        return maxPeople;
    }

    public String getPriceBegin() {
        return priceBegin;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public String getLocationName() {
        return locationName;
    }

    public String getLocationAddress() {
        return locationAddress;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(eventId);
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
