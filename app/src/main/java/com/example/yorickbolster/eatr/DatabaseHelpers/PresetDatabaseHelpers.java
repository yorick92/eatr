package com.example.yorickbolster.eatr.DatabaseHelpers;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.yorickbolster.eatr.HelperClasses.DispatchGroup;
import com.example.yorickbolster.eatr.RoomDatabase.AppDatabase;
import com.example.yorickbolster.eatr.RoomDatabase.MyContacts;
import com.example.yorickbolster.eatr.RoomDatabase.Preset;
import com.example.yorickbolster.eatr.RoomDatabase.PresetsTimeLastChanged;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class PresetDatabaseHelpers {
    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference refUser;
    private DispatchGroup DispatchPresets;

    private AppDatabase dbMyContacts, dbPresets, dbPresetsTLC;

    public PresetDatabaseHelpers(Context context) {
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();

        firebaseDatabase = FirebaseDatabase.getInstance();
        refUser = firebaseDatabase.getReference("Users").child(currentUser.getUid());
        dbMyContacts = Room.databaseBuilder(context, AppDatabase.class, "mycontacts").build();
        dbPresets = Room.databaseBuilder(context, AppDatabase.class, "Preset").build();
        dbPresetsTLC = Room.databaseBuilder(context, AppDatabase.class, "PresetTimeLastChanged").build();
    }

    private boolean presetsInfoChanged(String timeLastChanged){
        try{
            if(timeLastChanged == null){
                return true;
            }
            String presetsTLC = new RoomGetPresetsTLC().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR).get();
            if(presetsTLC != null){
                if(presetsTLC.equals(timeLastChanged)){
                    return false;
                }
                else{
                    return true;
                }
            }
            else{
                return true;
            }
        }
        catch(Exception e){
            Log.e("Exception", "contactInfoChangedException:" + e);
            return  false;
        }
    }

    private void loadPresetsFromFireBase(){
        refUser.child("presets").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ArrayList<Preset> presetList = new ArrayList<>();
                for (DataSnapshot presetSnapshot : dataSnapshot.getChildren()){
                    ArrayList<String> contactIds = new ArrayList<>();
                    for(DataSnapshot idSS : presetSnapshot.child("contacts").getChildren()){
                        contactIds.add(String.valueOf(idSS.getKey()));
                    }
                    Preset localPreset = new Preset(
                            presetSnapshot.getKey(),
                            String.valueOf(presetSnapshot.child("name").getValue()),
                            contactIds
                    );
                    presetList.add(localPreset);
                }
                new RoomSavePresets().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, presetList.toArray(new Preset[presetList.size()]));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                DispatchPresets.leave();
            }
        });
    }

    public void syncAllPresetData(DispatchGroup PassedDispatchGroup){
        DispatchPresets = PassedDispatchGroup;
        DispatchPresets.leave();
        refUser.child("presets").child("timeLastChanged").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Object timeLastChanged = dataSnapshot.getValue();
                if(timeLastChanged != null){
                    if(presetsInfoChanged(timeLastChanged.toString())){
                        loadPresetsFromFireBase();
                    }
                    else {
                        DispatchPresets.leave();
                    }
                }
                else {
                    loadPresetsFromFireBase();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    private class RoomSavePresets extends AsyncTask<Preset, Void, Void> {

        @Override
        protected Void doInBackground(Preset... presets) {
            if(presets.length > 0){
                dbPresets.presetsDao().insertAll(presets);
            }
            DispatchPresets.leave();
            return null;
        }
    }

    public class RoomGetPresetById extends AsyncTask<String, Void, Preset>{

        @Override
        protected Preset doInBackground(String... strings) {
            if(strings.length < 1){
                return null;
            }
            List<Preset> localPresetList = dbPresets.presetsDao().loadById(strings[0]);
            if(localPresetList.size() > 0){
                List<String> localContactIdsList = localPresetList.get(0).getContactIds();
                String[] localContactIdsArray = localContactIdsList.toArray(new String[localContactIdsList.size()]);
                List<MyContacts> contacts = dbMyContacts.myContactsDao().loadByIds(localContactIdsArray);
                localPresetList.get(0).setContacts(contacts);
                return localPresetList.get(0);
            }
            else{
                return null;
            }
        }
    }

    RoomGetPresetByIds getRoomGetPresetByIds(){
        return new RoomGetPresetByIds();
    }

    public class RoomGetPresetByIds extends AsyncTask<String, Void, List<Preset>>{

        @Override
        protected List<Preset> doInBackground(String... strings) {
            List<Preset> localPresetList = dbPresets.presetsDao().loadByIds(strings);
            for(int i = 0; i < localPresetList.size(); i++){
                List<String> localContactIdsList = localPresetList.get(i).getContactIds();
                String[] localContactIdsArray = localContactIdsList.toArray(new String[localContactIdsList.size()]);
                List<MyContacts> contacts = dbMyContacts.myContactsDao().loadByIds(localContactIdsArray);
                localPresetList.get(i).setContacts(contacts);
            }
            return localPresetList;
        }
    }

    RoomGetPresets getRoomGetPresets(){
        return new RoomGetPresets();
    }

    public class RoomGetPresets extends AsyncTask<Preset, Void, List<Preset>>{

        @Override
        protected List<Preset> doInBackground(Preset... presets) {
            List<Preset> localPresetList = dbPresets.presetsDao().getAll();
            for(int i = 0; i < localPresetList.size(); i++){
                List<String> localContactIdsList = localPresetList.get(i).getContactIds();
                String[] localContactIdsArray = localContactIdsList.toArray(new String[localContactIdsList.size()]);
                List<MyContacts> contacts = dbMyContacts.myContactsDao().loadByIds(localContactIdsArray);
                localPresetList.get(i).setContacts(contacts);
            }
            return localPresetList;
        }
    }

    RoomGetPresetsTLC getRoomGetPresetsTLC(){
        return new RoomGetPresetsTLC();
    }

    public class RoomGetPresetsTLC extends AsyncTask<Void, Void, String>{

        @Override
        protected String doInBackground(Void... voids) {
            List<PresetsTimeLastChanged> localList =  dbPresetsTLC.presetTLCDao().getAll();
            if(localList.size() < 1){
                return null;
            }
            else{
                return localList.get(0).getTimeLastChanged();
            }
        }
    }

    public RoomDeleteAllPresets getRoomDeleteAllPresets(){
        return new RoomDeleteAllPresets();
    }

    public class RoomDeleteAllPresets extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            dbPresets.presetsDao().deleteAll();
            dbPresets.presetTLCDao().deleteAll();
            return null;
        }
    }
}
