package com.example.yorickbolster.eatr.DatabaseHelpers;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.yorickbolster.eatr.RoomDatabase.AllContacts;
import com.example.yorickbolster.eatr.RoomDatabase.AppGroup;
import com.example.yorickbolster.eatr.RoomDatabase.MyContacts;
import com.example.yorickbolster.eatr.RoomDatabase.Preset;
import com.example.yorickbolster.eatr.RoomDatabase.User;

import java.util.List;

/**
 * Created by yorickbolster on 22/07/2018.
 */

public class DeleteAllDataHelpers {

    private UserDatabaseHelpers userDatabaseHelpers;
    private ContactsDatabaseHelpers contactsDatabaseHelpers;
    private PresetDatabaseHelpers presetDatabaseHelpers;
    private GroupDatabaseHelpers groupDatabaseHelpers;
    private EventsDatabaseHelpers eventsDatabaseHelpers;

    public final Runnable deleteAll;
    private final Runnable deleteUser;
    private final Runnable deleteMyContacts;
    private final Runnable deletePresets;
    private final Runnable deleteGroups;
    private final Runnable deleteAllContacts;
    private final Runnable deleteEvents;

    public DeleteAllDataHelpers(Context context){
        userDatabaseHelpers = new UserDatabaseHelpers(context);
        contactsDatabaseHelpers = new ContactsDatabaseHelpers(context);
        presetDatabaseHelpers = new PresetDatabaseHelpers(context);
        groupDatabaseHelpers = new GroupDatabaseHelpers(context);

        deleteUser = new Runnable() {
            @Override
            public void run() {
                try{
                    userDatabaseHelpers.getRoomDeleteUser().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                catch(Exception e){
                    Log.e("RoomDatabaseExceptions", "An exception occured: " + e);
                }
            }
        };

        deleteMyContacts = new Runnable() {
            @Override
            public void run() {
                Log.d("TestRoomData", "deleteMyContacts: ");
                try{
                    contactsDatabaseHelpers.getRoomDeleteAllMyContacts().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                catch(Exception e){
                    Log.e("RoomDatabaseExceptions", "An exception occured: " + e);
                }
            }
        };

        deletePresets = new Runnable() {
            @Override
            public void run() {
                Log.d("TestRoomData", "deletePresets: ");
                try{
                    presetDatabaseHelpers.getRoomDeleteAllPresets().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                catch(Exception e){
                    Log.e("RoomDatabaseExceptions", "An exception occured: " + e);
                }

            }
        };

        deleteGroups = new Runnable() {
            @Override
            public void run() {
                Log.d("TestRoomData", "deleteGroups: ");
                try{
                    groupDatabaseHelpers.getRoomDeleteAllGroups().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                catch(Exception e){
                    Log.e("RoomDatabaseExceptions", "An exception occured: " + e);
                }
            }
        };

        deleteAllContacts = new Runnable() {
            @Override
            public void run() {
                Log.d("TestRoomData", "deleteAllContacts: ");
                try{
                    contactsDatabaseHelpers.getRoomDeleteAllAllContacts().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                catch(Exception e){
                    Log.e("RoomDatabaseExceptions", "An exception occured: " + e);
                }
            }
        };

        deleteEvents = new Runnable() {
            @Override
            public void run() {
                Log.d("TestRoomData", "deleteEvents: ");
                try{
                    eventsDatabaseHelpers.getDeleteAllEvents().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                catch(Exception e){
                    Log.e("RoomDatabaseExceptions", "An exception occured: " + e);
                }
            }
        };

        deleteAll = new Runnable() {
            @Override
            public void run() {
                try{
                    deleteUser.run();
                    deleteMyContacts.run();
                    deletePresets.run();
                    deleteGroups.run();
                    deleteAllContacts.run();
                    deleteEvents.run();
                }
                catch(Exception e){
                    Log.e("RoomDatabaseExceptions", "An exception occured: " + e);
                }
            }
        };
    }
}
