package com.example.yorickbolster.eatr.ServerSync;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.yorickbolster.eatr.RoomDatabase.AppDatabase;
import com.example.yorickbolster.eatr.RoomDatabase.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

public class FirebaseSync extends AsyncTask {

    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private DatabaseReference databaseReference;
    private StorageReference mStorageRef;
    private DatabaseReference facebookFriendsRef;

    public ArrayList<String> facebookFriendsArray;

    AppDatabase dbUsers;

    public FirebaseSync(Context appContext) {
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        mStorageRef = FirebaseStorage.getInstance().getReference();
        facebookFriendsRef = databaseReference.child("Users").child(currentUser.getUid()).child("facebookFriends");
        facebookFriendsArray = new ArrayList<>();

        // TODO checken of het problematisch is dat er meerdere contexten meegegeven worden (afhankelijk van plaats initialisatie)
        dbUsers = Room.databaseBuilder(appContext, AppDatabase.class, "users").build();
    }

    public void testDatabase()
    {
        databaseReference.child("Users").child(currentUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("friends database", String.valueOf(dataSnapshot.child("email").getValue()));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void getFriends() {
        // TODO in User.java staat ook een lijstobject met FacebookFriends

        facebookFriendsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
//                facebookFriendsArray.clear();
//                Log.d("Friends array - voor", facebookFriendsArray.toString());
                for (DataSnapshot eventSnapShot : dataSnapshot.getChildren()) {
                    facebookFriendsArray.add(eventSnapShot.child("id").getValue().toString());
                }
//                Log.d("Friends array - na", facebookFriendsArray.toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("DatabaseError", databaseError.toString());
            }
        });
    }

    public void getUserData()
    {
        databaseReference.child(currentUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                dbUsers.userDao().insertAll(new User(
                        dataSnapshot.getKey(),
                        dataSnapshot.child("email").getValue().toString(),
                        dataSnapshot.child("facebookFriends").getValue().toString(),
                        dataSnapshot.child("facebookId").getValue().toString(),
                        dataSnapshot.child("facebookPicUrl").getValue().toString(),
                        dataSnapshot.child("favCuis").getValue().toString(),
                        dataSnapshot.child("favRest").getValue().toString(),
                        dataSnapshot.child("gender").getValue().toString(),
                        dataSnapshot.child("groups").getValue().toString(),
                        dataSnapshot.child("joinedEvents").getValue().toString(),
                        dataSnapshot.child("myEvents").getValue().toString(),
                        dataSnapshot.child("name").getValue().toString(),
                        dataSnapshot.child("picChangedAmount").getValue().toString(),
                        dataSnapshot.child("presets").getValue().toString(),
                        dataSnapshot.child("timeLastChanged").getValue().toString()
                ));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        return null;
    }
}
