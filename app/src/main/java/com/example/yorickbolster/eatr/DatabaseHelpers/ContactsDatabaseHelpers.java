package com.example.yorickbolster.eatr.DatabaseHelpers;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.yorickbolster.eatr.HelperClasses.DispatchGroup;
import com.example.yorickbolster.eatr.RoomDatabase.AllContacts;
import com.example.yorickbolster.eatr.RoomDatabase.AppDatabase;
import com.example.yorickbolster.eatr.RoomDatabase.MyContacts;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yorickbolster on 19/07/2018.
 */

public class ContactsDatabaseHelpers {
    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference refUser;
    private DatabaseReference refPublicProfiles;

    private AppDatabase dbMyContacts;
    private AppDatabase dbAllContacts;
    private DispatchGroup DispatchContacts;
    private List<MyContacts> TempMyContactList;
    private List<AllContacts> TempAllContactList;

    boolean startUp = true;

    public ContactsDatabaseHelpers(Context context){
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();

        firebaseDatabase = FirebaseDatabase.getInstance();
        refUser = firebaseDatabase.getReference("Users").child(currentUser.getUid());
        refPublicProfiles = firebaseDatabase.getReference("publicProfiles");

        dbMyContacts = Room.databaseBuilder(context, AppDatabase.class, "mycontacts").build();
        dbAllContacts = Room.databaseBuilder(context, AppDatabase.class, "allcontacts").build();
    }

    private boolean myContactInfoChanged(String id, String timeLastChanged){
        try{
            MyContacts contact = new RoomGetMyContactById().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, id).get();
            if(contact != null){
                if(contact.getTimeLastChanged().equals(timeLastChanged)){
                    return false;
                }
                else{
                    return true;
                }
            }
            else{
                return true;
            }
        }
        catch(Exception e){
            Log.e("Exception", "contactInfoChangedException:" + e);
            return  false;
        }
    }

    private void loadMyContactFromDatabase(DatabaseReference refPublicProfiles, String friendId, final DispatchGroup gotAllContacts){
        refPublicProfiles.child(friendId).addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot2) {

                MyContacts myContact = new MyContacts(
                        dataSnapshot2.getKey(),
                        String.valueOf(dataSnapshot2.child("favCuis").getValue()),
                        String.valueOf(dataSnapshot2.child("favRest").getValue()),
                        String.valueOf(dataSnapshot2.child("gender").getValue()),
                        String.valueOf(dataSnapshot2.child("name").getValue()),
                        String.valueOf(dataSnapshot2.child("picChangedAmount").getValue()),
                        String.valueOf(dataSnapshot2.child("timeLastChanged").getValue())
                );

                AllContacts allContact = new AllContacts(
                        dataSnapshot2.getKey(),
                        String.valueOf(dataSnapshot2.child("favCuis").getValue()),
                        String.valueOf(dataSnapshot2.child("favRest").getValue()),
                        String.valueOf(dataSnapshot2.child("gender").getValue()),
                        String.valueOf(dataSnapshot2.child("name").getValue()),
                        String.valueOf(dataSnapshot2.child("picChangedAmount").getValue()),
                        String.valueOf(dataSnapshot2.child("timeLastChanged").getValue())
                );

                if(startUp){
                    TempMyContactList.add(myContact);
                    TempAllContactList.add(allContact);
                    DispatchContacts.leave();
                    gotAllContacts.leave();
                }
                else{
                    new RoomSaveMyContacts().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, myContact);
                    new RoomSaveAllContacts().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, allContact);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                DispatchContacts.leave();
                gotAllContacts.leave();
            }
        });
    }




    public void syncMyContactsData(final DispatchGroup dispatchContactsPassed) {
        DispatchContacts = dispatchContactsPassed;
        refUser.child("facebookFriends").addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                TempMyContactList = new ArrayList<>();
                TempAllContactList = new ArrayList<>();
                final DispatchGroup gotAllContacts = new DispatchGroup("gotAllContacts");

                for (DataSnapshot friendSnapshot : dataSnapshot.getChildren()){
                    gotAllContacts.enter();
                    DispatchContacts.enter();
                    final String friendId = friendSnapshot.getKey();
                    refPublicProfiles.child(friendId).child("timeLastChanged").addValueEventListener(new ValueEventListener() {

                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot2) {
                            if(myContactInfoChanged(friendId, dataSnapshot2.getValue().toString())){

                                Log.d("FireDatabaseDebug", "contact " + friendId + " changed");
                                loadMyContactFromDatabase(refPublicProfiles, friendId, gotAllContacts);
                            }
                            else{
                                Log.d("FireDatabaseDebug", "contact " + friendId + " unchanged");
                                DispatchContacts.leave();
                                gotAllContacts.leave();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            DispatchContacts.leave();
                            gotAllContacts.leave();
                        }
                    });

                }

                final Runnable saveContacts = new Runnable() {
                    @Override
                    public void run() {
                        new RoomSaveAllContacts().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, TempAllContactList.toArray(new AllContacts[TempAllContactList.size()]));
                        new RoomSaveMyContacts().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, TempMyContactList.toArray(new MyContacts[TempMyContactList.size()]));
                    }
                };

                gotAllContacts.notify(saveContacts);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                DispatchContacts.leave();
            }
        });
    }

    /**** MY CONTACTS ****/

    private class RoomSaveMyContacts extends AsyncTask<MyContacts, Void, Void> {

        @Override
        protected Void doInBackground(MyContacts... contacts) {

            if (contacts.length > 0) {
                dbMyContacts.myContactsDao().insertAll(contacts);
                if(startUp){
                    DispatchContacts.leave();
                }
                startUp = false;
            }
            else {
                Log.d("RoomDatabaseDebug", "ContactsDatabaseHelpers you passed an empty list of MyContacts");
                DispatchContacts.leave();
            }
            return null;
        }
    }

    public RoomGetMyContactById getRoomGetMyContactById(){
        return new RoomGetMyContactById();
    }

    public class RoomGetMyContactById extends AsyncTask<String, Void, MyContacts> {
        @Override
        protected MyContacts doInBackground(String... strings) {
            List<MyContacts> contactsList = dbMyContacts.myContactsDao().loadById(strings[0]);
            if(contactsList.size() > 0){
                return contactsList.get(0);
            }
            else{
                Log.d("RoomDatabaseDebug", "ContactsDatabaseHelpers you passed an empty list of Ids");
                return null;
            }
        }
    }

    public RoomGetContactsByIds getRoomGetContactsByIds(){
        return new RoomGetContactsByIds();
    }

    public class RoomGetContactsByIds extends AsyncTask<String, Void, List<MyContacts>> {
        @Override
        protected List<MyContacts> doInBackground(String... strings) {
            List<MyContacts> contactsList = dbMyContacts.myContactsDao().loadByIds(strings);
            return contactsList;
        }
    }

    public RoomGetAllMyContacts getRoomGetAllMyContacts(){
        return new RoomGetAllMyContacts();
    }

    public class RoomGetAllMyContacts extends AsyncTask<Void, Void, List<MyContacts>> {
        @Override
        public List<MyContacts> doInBackground(Void... voids) {
            List<MyContacts> aList = dbMyContacts.myContactsDao().getAll();
            if(aList.size() < 1){
                Log.d("RoomDatabaseDebug", "ContactsDatabaseHelpers you have no contacts saved in MyContacts: " + aList );
            }
            return aList;
        }
    }

    public RoomDeleteAllMyContacts getRoomDeleteAllMyContacts(){
        return new RoomDeleteAllMyContacts();
    }

    public class RoomDeleteAllMyContacts extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            dbMyContacts.myContactsDao().deleteAll();
            return null;
        }
    }

    /**** ALL CONTACTS ****/

    public RoomSaveAllContacts getRoomSaveAllContacts(){
        return new RoomSaveAllContacts();
    }

    public class RoomSaveAllContacts extends AsyncTask<AllContacts, Void, Void>{

        @Override
        protected Void doInBackground(AllContacts... allContacts) {
            if (allContacts.length > 0) {
                dbAllContacts.allContactsDao().insertAll(allContacts);
            }
            else {
                Log.d("RoomDatabaseDebug", "ContactsDatabaseHelpers you passed an empty list of AllContacts");
                DispatchContacts.leave();
            }
            return null;
        }
    }

    public RoomGetAllAllContacts getRoomGetAllAllContacts(){
        return new RoomGetAllAllContacts();
    }

    public class RoomGetAllAllContacts extends AsyncTask<Void, Void, List<AllContacts>>{

        @Override
        protected List<AllContacts> doInBackground(Void... voids) {
            List<AllContacts> aList = dbAllContacts.allContactsDao().getAll();
            if(aList.size() < 1){
                Log.d("RoomDatabaseDebug", "ContactsDatabaseHelpers you have no contacts saved in AllContacts: " + aList );
            }
            return aList;
        }
    }

    public RoomGetAllContactsByIds getRoomGetAllContactsByIds(){
        return new RoomGetAllContactsByIds();
    }

    public class RoomGetAllContactsByIds extends AsyncTask<String, Void, List<AllContacts>>{

        @Override
        protected List<AllContacts> doInBackground(String... strings) {
            List<AllContacts> aList = dbAllContacts.allContactsDao().loadByIds(strings);
            if(aList.size() < 1){
                Log.d("RoomDatabaseDebug", "ContactsDatabaseHelpers you have no contacts saved in AllContacts: " + aList );
            }
            return aList;
        }
    }

    public RoomGetAllContactById getRoomGetAllContactById(){
        return new RoomGetAllContactById();
    }

    public class RoomGetAllContactById extends AsyncTask<String, Void, AllContacts>{

        @Override
        protected AllContacts doInBackground(String... strings) {
            List<AllContacts> aList = dbAllContacts.allContactsDao().loadById(strings[0]);

            if(aList.size() > 0){
                return aList.get(0);
            }
            else{
                Log.d("RoomDatabaseDebug", "ContactsDatabaseHelpers you have no contacts saved in AllContacts: " + aList );
                return  null;
            }
        }
    }

    public RoomDeleteAllAllContacts getRoomDeleteAllAllContacts(){
        return new RoomDeleteAllAllContacts();
    }

    public class RoomDeleteAllAllContacts extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            dbAllContacts.allContactsDao().deleteAll();
            return null;
        }
    }
}
