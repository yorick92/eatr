package com.example.yorickbolster.eatr.AddEventViewPager;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.DimenRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class ItemOffsetDecoration extends RecyclerView.ItemDecoration {

    private int mItemOffset;

    public ItemOffsetDecoration(int itemOffset) {
        mItemOffset = itemOffset;
    }

    public ItemOffsetDecoration(@NonNull Context context, @DimenRes int itemOffsetId) {
        this(context.getResources().getDimensionPixelSize(itemOffsetId));
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        int position = parent.getChildViewHolder(view).getAdapterPosition();
        int itemCount = state.getItemCount();

//        outRect.set(mItemOffset, mItemOffset, mItemOffset, mItemOffset);
        outRect.left = mItemOffset;
        outRect.right = position == itemCount - 1 ? mItemOffset : 0; // states: if not last item, then outRect.right = 0. To compensate for outRect.left
        outRect.top = mItemOffset;
        outRect.bottom = mItemOffset;
    }
}