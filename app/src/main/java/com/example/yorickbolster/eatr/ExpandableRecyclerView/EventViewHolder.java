package com.example.yorickbolster.eatr.ExpandableRecyclerView;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yorickbolster.eatr.EventItem;
import com.example.yorickbolster.eatr.ImageTransformations.BlurTransformation;
import com.example.yorickbolster.eatr.ImageTransformations.BrightnessFilterTransformation;
import com.example.yorickbolster.eatr.R;
import com.squareup.picasso.Picasso;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

public class EventViewHolder extends ChildViewHolder {

    private TextView locationName, creatorName, eventDate, eventPrice;
    private ImageView eventPic, creatorPic;

    public EventViewHolder(View itemView) {
        super(itemView);
        locationName = itemView.findViewById(R.id.locationView);
        eventPic = itemView.findViewById(R.id.locationPic);
        creatorPic = itemView.findViewById(R.id.creatorImageView2);
        creatorName = itemView.findViewById(R.id.creatorView);
        eventDate = itemView.findViewById(R.id.dateView);
        eventPrice = itemView.findViewById(R.id.priceRangeView);
    }

    public void onBind(EventItem eventItem, Context appContext) {
        locationName.setText(eventItem.getLocationName());
        Picasso.with(appContext).load(eventItem.getEventPicture()).transform(new BlurTransformation(appContext)).transform( new BrightnessFilterTransformation(appContext, -0.3f)).into(eventPic);
        Log.e("eventItem.getEventPictu", String.valueOf(eventItem.getEventPicture()));
        Picasso.with(appContext).load(eventItem.getCreatorPicture()).into(creatorPic);
        creatorName.setText(eventItem.getCreator());
        eventDate.setText(eventItem.getDateStart().toString());
        eventPrice.setText(eventItem.getPriceBegin() + " - " + eventItem.getPriceEnd());
    }
}