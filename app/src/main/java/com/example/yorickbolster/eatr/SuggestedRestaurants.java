package com.example.yorickbolster.eatr;

/**
 * Created by yorickbolster on 03/03/2018.
 */

public class SuggestedRestaurants {
    private String restaurantName;
    private String locationId;
    private String rating;
    private String distance;
    private String vicinity;
    private String longitude;
    private String photoUrlString;

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    private String latitude;

    public String getPhotoUrlString() {
        return photoUrlString;
    }

    public void setPhotoUrlString(String photoUrlString) {
        this.photoUrlString = photoUrlString;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public String getLocationId() {
        return locationId;
    }

    public String getRating() {
        return rating;
    }

    public String getDistance() {
        return distance;
    }

    public String getVicinity() {
        return vicinity;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }
}
