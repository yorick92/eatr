package com.example.yorickbolster.eatr.RoomDatabase;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface AppGroupDao {
    @Query("SELECT * FROM AppGroup")
    List<AppGroup> getAll();

    @Query("SELECT * FROM AppGroup WHERE id IN (:groupIds)")
    List<AppGroup> loadByIds(String... groupIds);

    @Query("SELECT * FROM AppGroup WHERE id IN (:groupIds)")
    List<AppGroup> loadById(String groupIds);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(AppGroup... appGroups);

    @Query("DELETE FROM AppGroup")
    void deleteAll();

    @Update
    void updatePreset(AppGroup... appGroups);

    @Delete
    void delete(AppGroup appGroup);

    @Query("DELETE FROM AppGroup")
    void deleteTable();
}