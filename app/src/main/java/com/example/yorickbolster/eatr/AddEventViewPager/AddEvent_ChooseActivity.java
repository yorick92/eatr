package com.example.yorickbolster.eatr.AddEventViewPager;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.yorickbolster.eatr.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AddEvent_ChooseActivity extends AppCompatActivity {

    private Context context;
    private RecyclerView rvGetCoffee, rvEatOut;
    private LinearLayoutManager layoutManagerGetCoffee, layoutManagerEatOut;
    private HorizontalRecyclerViewAdapter adapterCoffee, adapterEatOut;
    private List<Object> listCoffee, listEatOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event_choose_activity);
        context = this;

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rvGetCoffee = findViewById(R.id.rvGetCoffee);
        rvEatOut = findViewById(R.id.rvEatOut);

        layoutManagerGetCoffee = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvGetCoffee.setLayoutManager(layoutManagerGetCoffee);
        layoutManagerEatOut = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvEatOut.setLayoutManager(layoutManagerEatOut);

        ItemOffsetDecoration dividerItemDecoration = new ItemOffsetDecoration(context, R.dimen.item_offset);
        rvGetCoffee.addItemDecoration(dividerItemDecoration);
        rvEatOut.addItemDecoration(dividerItemDecoration);

        rvGetCoffee.setHasFixedSize(true);
        rvEatOut.setHasFixedSize(true);

        init();
    }

    public void init() {

        // coffee
        listCoffee = new ArrayList<>();
        adapterCoffee = new HorizontalRecyclerViewAdapter(context);
        rvGetCoffee.setAdapter(adapterCoffee);

        DatabaseReference refFeaturedPlaces = FirebaseDatabase.getInstance().getReference().child("featured_places");
        refFeaturedPlaces.child("52,1601-4,4970").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot placeSnapShot : dataSnapshot.getChildren()) {
                    Places place;

                    String placeId = String.valueOf(placeSnapShot.getValue());

                    place = getPlaceDetails(placeId);

                    place.setFeaturedTimestamp(placeSnapShot.getKey());
                    place.setId(placeId);

                    // TODO fill in rest of place
//                    place.setName();
//                    place.setAddressStreetNumber();
//                    place.setImage();
//                    place.setRating();
//                    place.setPlaceType();
//                    place.setTravelDistance();


                    listCoffee.add(place);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        listCoffee.add(new SearchPlace());
        listCoffee.add(new Places());
        listCoffee.add(new Places());
        listCoffee.add(new Places());
        listCoffee.add(new Places());
        listCoffee.add(new Places());

        adapterCoffee.setDataSource(listCoffee);
        adapterCoffee.notifyDataSetChanged();


        // eat out
        listEatOut = new ArrayList<>();
        adapterEatOut = new HorizontalRecyclerViewAdapter(context);
        rvEatOut.setAdapter(adapterEatOut);

        listEatOut.add(new SearchPlace());
        listEatOut.add(new Places());
        listEatOut.add(new Places());
        listEatOut.add(new Places());
        listEatOut.add(new Places());

        adapterEatOut.setDataSource(listEatOut);
        adapterEatOut.notifyDataSetChanged();
    }

    public void getPlaceImage(Places place) {
//        String restPhotoRef = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + place.getPhotoUrlString() + "&key=AIzaSyCeLVUpx-gIibTYo10URfS1oyKNf72XbmM";
    }

    public static String getMetadata(Context context, String name) {
        try {
            ApplicationInfo appInfo = context.getPackageManager().getApplicationInfo(
                    context.getPackageName(), PackageManager.GET_META_DATA);
            if (appInfo.metaData != null) {
                return appInfo.metaData.getString(name);
            }
        } catch (PackageManager.NameNotFoundException e) {
// if we can’t find it in the manifest, just return null
        }

        return null;
    }

    public Places getPlaceDetails(String placeId) {
        Places place = new Places();
        String API_KEY = getMetadata(context, "com.google.android.geo.API_KEY");
        String parameters = "&fields=" + "name,rating,formatted_phone_number";

        String placeDetailsURL = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + placeId + parameters + "&key=" + API_KEY;

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, placeDetailsURL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("JSON api", String.valueOf(response));
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("tag", String.valueOf(error));
            }
        });

//        jsObjRequest.

//        place.setName();
//        place.setAddressStreetNumber();
//        place.setImage();
//        place.setRating();
//        place.setPlaceType();
//        place.setTravelDistance();

        return place;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // scroll back to top
        // else finish activity
        finish();
    }
}
