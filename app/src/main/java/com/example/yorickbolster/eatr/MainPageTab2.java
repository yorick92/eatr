package com.example.yorickbolster.eatr;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.yorickbolster.eatr.ExpandableRecyclerView.EventType;
import com.example.yorickbolster.eatr.ExpandableRecyclerView.EventTypeAdapter;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class MainPageTab2 extends Fragment {

    private Context appContext;
    private FirebaseAuth mAuth;
    private StorageReference mStorageRef;
    DatabaseReference databaseMyEvents, databaseMyEvents2;
    private EventTypeAdapter adapter;
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;

    private List<EventType> genres;
    private List<EventType> eventTypes;
    private List<EventItem> listJoinedEvents;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_main_page_tab2, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        appContext = getActivity();
        mAuth = FirebaseAuth.getInstance();
        // TODO reference aanpassen naar specifiek de user
        databaseMyEvents = FirebaseDatabase.getInstance().getReference("Events");
        recyclerView = getActivity().findViewById(R.id.recyclerViewYourEvents);
        layoutManager = new LinearLayoutManager(appContext);
        mStorageRef = FirebaseStorage.getInstance().getReference();



        ((MainPageTabbed) getActivity()).setFabScrollbehaviour(recyclerView);
    }

    private void attachAdapter(List<EventType> list)
    {
        adapter = new EventTypeAdapter(genres);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onStart() {
        super.onStart();

        //onderstaand is een kleine tryout om data te filteren. onderstaande code geeft lijst van
        // alle events waar rmpvXV... bij is
        databaseMyEvents2 = FirebaseDatabase.getInstance().getReference("Users").child("rpmvXVzwPLcMKRgWZz6dbjoJK5E3").child("myEvents");
        databaseMyEvents2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                collectEventIds((Map<String,Object>) dataSnapshot.getValue());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
    private void collectEventIds(Map<String,Object> myEventsSnapshot) {

        ArrayList<String> eventIds = new ArrayList<>();

        //iterate through each user, ignoring their UID
        for (Map.Entry<String, Object> entry : myEventsSnapshot.entrySet()){

            //Get value and append to list
            eventIds.add((String) entry.getValue());
        }

        System.out.println(eventIds.toString());
        getEvents(eventIds);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        adapter.onSaveInstanceState(outState);
    }

    private void getEvents(ArrayList<String> eventIds) {
        eventTypes = new ArrayList<>();
        listJoinedEvents = new ArrayList<>();
//            final List<EventItem> listCreatedEvents = new ArrayList<>();

        System.out.println("Attaching listener");
        databaseMyEvents.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listJoinedEvents.clear();
                for (final DataSnapshot eventSnapShot : dataSnapshot.getChildren()) {

                    // TODO events ophalen die in lijstje myEvents staan
//                    Log.e("eventsnapshot id", eventSnapShot.getValue().toString());

                    final EventItem joinedEvent = new EventItem(
                            eventSnapShot.child("eventId").getValue().toString(),
                            eventSnapShot.child("creator").getValue().toString(),
                            eventSnapShot.child("locationName").getValue().toString(),
                            eventSnapShot.child("locationAddress").getValue().toString(),
                            eventSnapShot.child("locationId").getValue().toString(),
                            new Date(Long.parseLong(eventSnapShot.child("miliseconds").getValue().toString())),
                            eventSnapShot.child("priceBegin").getValue().toString(),
                            eventSnapShot.child("priceEnd").getValue().toString(),
                            eventSnapShot.child("maxPeople").getValue().toString(),
                            eventSnapShot.child("joinedPeople").getChildren().toString());

                    mStorageRef.child("pics/" + "Events/" + eventSnapShot.child("eventId").getValue().toString() + "/eventPic.jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            joinedEvent.setEventPicture(uri);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.e("tab2 mStorageRef", "event has no picture");
                        }
                    });

                    mStorageRef.child("pics/" + "Users/" + eventSnapShot.child("creator").getValue().toString() + "/profile.jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            joinedEvent.setCreatorPicture(uri);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.e("tab2 mStorageRef", "creator has no profile picture. creatorId: " + eventSnapShot.child("creator").getValue().toString());
                        }
                    });

//                    Log.e("tab2 eventImages[0]", String.valueOf(eventImages[0]));
//                    Log.e("tab2 eventImages[1]", String.valueOf(eventImages[1]));


                    listJoinedEvents.add(joinedEvent);
//                    Log.e("eventImages[0]", eventImages[0].toString());
                }
                EventType joinedEvents = new EventType("Joined events (" + listJoinedEvents.size()+")", listJoinedEvents);
//                EventType createdEvents = new EventType("Created events", listCreatedEvents);

                eventTypes.add(joinedEvents);
//                eventTypes.add(createdEvents);
//                Log.e("genres", "is null");
                genres = eventTypes;
//                Log.e("genres", "not null, but =eventTypes");

                System.out.println("Received " + listJoinedEvents.size() + " items");

                attachAdapter(eventTypes);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("DatabaseError", databaseError.getDetails());
            }

        });
        System.out.println("Returning "+listJoinedEvents.size()+" items");
    }

}