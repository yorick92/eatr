package com.example.yorickbolster.eatr.RoomDatabase;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.example.yorickbolster.eatr.HelperClasses.Converters;

@Database(entities = {User.class, MyContacts.class, AllContacts.class, Preset.class, AppGroup.class, PresetsTimeLastChanged.class, Event.class}, version = 2, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract UserDao userDao();
    public abstract MyContactsDao myContactsDao();
    public abstract AllContactsDao allContactsDao();
    public abstract PresetsDao presetsDao();
    public abstract AppGroupDao groupsDao();
    public abstract PresetsTimeLastChangedDao presetTLCDao();
    public abstract EventDao eventDao();
}