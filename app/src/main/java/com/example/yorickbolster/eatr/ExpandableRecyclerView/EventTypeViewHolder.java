package com.example.yorickbolster.eatr.ExpandableRecyclerView;

import android.view.View;
import android.widget.TextView;

import com.example.yorickbolster.eatr.R;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

public class EventTypeViewHolder extends GroupViewHolder {

        private TextView eventTypeTitle;

        public EventTypeViewHolder(View itemView) {
            super(itemView);
            eventTypeTitle = itemView.findViewById(R.id.list_item_eventtype_name);
        }

        public void setEventTypeTitle(ExpandableGroup group) {
            eventTypeTitle.setText(group.getTitle());
        }
}